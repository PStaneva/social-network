function likePost(postId) {
    $.ajax({
        type: 'POST',
        // url: `http://localhost:8080/like/post/${postId}`,
        url: `http://localhost:8080/api/likes/post/${postId}`,
        dataType: 'json',
        async: false,
        success: function (data, textStatus, jqXHR) {
            $("#isUserLikedIt").attr("isUserLikedIt", "true");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // alert(textStatus);
        }
    }).then(r => {});
    getLikesOfPost(postId)
    location.reload()
}

function unlikePost(postId) {
    $.ajax({
        type: 'DELETE',
        // url: `http://localhost:8080/like/post/${postId}`,
        url: `http://localhost:8080/api/likes/post/${postId}`,
        dataType: 'json',
        async: false,
        success: function (data, textStatus, jqXHR) {
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // alert(textStatus);
        }
    }).then(r => {});
    getLikesOfPost(postId)
    location.reload()
}

// function likePost(postId) {
//     if (!isPostAlreadyLiked(postId)) {
//         $.ajax({
//             type: 'POST',
//             url: `http://localhost:8080/like/post/${postId}`,
//             dataType: 'json',
//             async: false,
//             success: function (data, textStatus, jqXHR) {
//             },
//             error: function (jqXHR, textStatus, errorThrown) {
//                 // alert(textStatus);
//             }
//         }).then(r => {});
//     } else {
//         $.ajax({
//             type: 'DELETE',
//             url: `http://localhost:8080/like/post/${postId}`,
//             dataType: 'json',
//             async: false,
//             success: function (data, textStatus, jqXHR) {
//             },
//             error: function (jqXHR, textStatus, errorThrown) {
//                 // alert(textStatus);
//             }
//         }).then(r => {});
//     }
//     getLikesOfPost(postId)
//     location.reload()
// }

function getLikesOfPost(postId) {
    let likesCount = 0;
    $.ajax({
        async: false,
        type: 'GET',
        url: `http://localhost:8080/like/post/count/${postId}`,
        dataType: 'json',
        success: function (likes) {
            likesCount = likes;
        }
    }).then(r => {});
}

function isPostAlreadyLiked(postId) {
    let alreadyLiked = false;
    $.ajax({
        async: false,
        type: 'GET',
        url: `http://localhost:8080/like/post/exist/${postId}`,
        dataType: 'json',
        success: function (result) {
            alreadyLiked = result;
        }
    }).then(r => {});
    return alreadyLiked;
}