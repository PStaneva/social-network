package com.assn.demo.api.mvc;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Post;
import com.assn.demo.models.PostDTO;
import com.assn.demo.models.UserInfo;
import com.assn.demo.services.CommentService;
import com.assn.demo.services.LikeService;
import com.assn.demo.services.PostService;
import com.assn.demo.services.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Controller
@RequestMapping("/post")
public class PostController {

    private PostService postService;
    private UserInfoService userInfoService;
    private CommentService commentService;
    private LikeService likeService;

    @Autowired
    public PostController(PostService postService, UserInfoService userInfoService, CommentService commentService, LikeService likeService) {
        this.postService = postService;
        this.userInfoService = userInfoService;
        this.commentService = commentService;
        this.likeService = likeService;
    }

    @GetMapping("/{id}")
    public String getPostById(@Valid @PathVariable UUID id, Principal principal, Model model) {
        getPostView(id, principal, model, postService, commentService, userInfoService, likeService);
        return "post-read";
    }

    @GetMapping("/create-form")
    public String showCreatePostForm(Model model) {
        model.addAttribute("postDTO", new PostDTO());
        return "post-create";
    }

    @PostMapping("/create")
    public String createPost(@ModelAttribute("postDTO") @Valid PostDTO postDTO,
                             Principal principal,
                             Model model) {
        UserInfo userInfo = userInfoService.getByUsername(principal.getName());
        postService.create(postDTO, userInfo);
        UUID postId = postService.getAllPostsByUser(userInfo.getId()).get(0).getId();
        getPostView(postId, principal, model, postService, commentService, userInfoService, likeService);
        return "post-read";
    }

    @GetMapping("/update-form/{id}")
    public String showUpdatePostForm(@PathVariable UUID id, Model model) {
        Post postToUpdate = postService.getById(id);
        PostDTO postDTO = new PostDTO();
        postDTO.setTitle(postToUpdate.getTitle());
        postDTO.setContent(postToUpdate.getContent());
        postDTO.setLocation(postToUpdate.getLocation());
        model.addAttribute("postId", id);
        model.addAttribute("postDTO", postDTO);
        return "post-update";
    }

    @PostMapping("/update/{id}")
    public String updatePost(@PathVariable UUID id,
                             @ModelAttribute("postDTO") @Valid PostDTO postDTO,
                             Model model,
                             Principal principal) {
        UserInfo userInfo = userInfoService.getByUsername(principal.getName());
        checkImageNotNull(id, postDTO);
        postService.update(id, postDTO, userInfo);
        getPostView(id, principal, model, postService, commentService, userInfoService, likeService);
        return "post-read";
    }

    @PostMapping("/{id}")
    public String delete(@PathVariable UUID id, Principal principal, Model model) {
        UserInfo userInfo = userInfoService.getByUsername(principal.getName());
        postService.delete(id, userInfoService.getByUsername(principal.getName()));
        model.addAttribute("user", userInfo);
        model.addAttribute("feed", postService.getAllPostsByUser(userInfo.getId()));
        return "redirect:/index";
    }

    @GetMapping("/all")
    public String showAllPosts(Model model, Principal principal) {
        if (IsUserAuthorized(principal)) {
            model.addAttribute("feed",
                    postService.getAllPostFeed(userInfoService.getByUsername(principal.getName()).getId()));
        } else {
            model.addAttribute("feed", postService.getAllPublicVisibleUsersPosts());
        }
        return "posts-list";
    }

    @GetMapping("/sorted/date/asc")
    public String showPostsSortedByDateAsc(Model model) {
        model.addAttribute("feed", postService.showPostsSortedByDateAsc());
        return "posts-list";
    }

    @GetMapping("/sorted/date/desc")
    public String showPostsSortedByDateDesc(Model model) {
        model.addAttribute("feed", postService.showPostsSortedByDateDesc());
        return "posts-list";
    }

    @ModelAttribute("users")
    public List<UserInfo> populateUsers() {
        return userInfoService.getAll();}

    @ModelAttribute("userService")
    public UserInfoService populateUserService() {
        return userInfoService;
    }

    private void checkImageNotNull(UUID id, PostDTO postDTO) {
        if (postDTO.getPicture() == null) postDTO.setPicture(postService.getById(id).getPicture());
    }

    static void getPostView(@PathVariable @Valid UUID id, Principal principal, Model model,
                            PostService postService, CommentService commentService, UserInfoService userInfoService,
                            LikeService likeService) {
        try {
            Post post = postService.getById(id);
            model.addAttribute("isThereNoPicture", post.getPicture() == null);
            model.addAttribute("post", post);
            model.addAttribute("comments", commentService.getAllCommentsByPost(id));
            model.addAttribute("postService", postService);
            model.addAttribute("isUserLikedIt",
                    likeService.isPostLikedByUser(id, userInfoService.getByUsername(principal.getName())));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    private boolean IsUserAuthorized(Principal principal) {
        return principal != null;
    }
}
