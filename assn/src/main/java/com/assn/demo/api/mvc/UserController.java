package com.assn.demo.api.mvc;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.InvalidUserOperation;
import com.assn.demo.models.Mapper;
import com.assn.demo.models.UserInfo;
import com.assn.demo.models.UserInfoDTO;
import com.assn.demo.models.UserInfoDetailsDTO;
import com.assn.demo.services.*;
import com.assn.demo.utils.Constants;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.security.Principal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.UUID;

@Controller
@RequestMapping("/user")
public class UserController {

    private UserInfoService userInfoService;
    private PostService postService;
    private RelationshipService relationshipService;
    private Mapper mapper;
    private LikeService likeService;
    private GenderService genderService;
    private ProfileVisibilityService profileVisibilityService;

    @Autowired
    public UserController(UserInfoService userInfoService,
                          PostService postService,
                          RelationshipService relationshipService,
                          Mapper mapper,
                          LikeService likeService,
                          GenderService genderService,
                          ProfileVisibilityService profileVisibilityService) {
        this.userInfoService = userInfoService;
        this.postService = postService;
        this.relationshipService = relationshipService;
        this.mapper = mapper;
        this.likeService = likeService;
        this.genderService = genderService;
        this.profileVisibilityService = profileVisibilityService;
    }

    @GetMapping("/all")
    public String showUsers(@NotNull Model model, Principal principal) {
        if (isUserLogged(principal)) {
            model.addAttribute("users", userInfoService.getAll());
        } else {
            model.addAttribute("users", userInfoService.getAllPublicVisibleUsers());
        }
        return "users-list";
    }

    @GetMapping("/search")
    public String showUserSearchResult(@RequestParam(required = false) String names, Model model, Principal principal) {
        try {
            if (!names.isEmpty()) {
                String[] searchSet = names.split(" ");
                final boolean ifFullNameSearch = searchSet.length >= 2;
                if (ifFullNameSearch) {
                    model.addAttribute("users", userInfoService.getAllUsersByFullName(searchSet[0], searchSet[1]));
                } else {
                    //email search
                    if (cannotBeFoundByEmail(names)) return "/";
                    UserInfo userInfo = userInfoService.getByEmail(names);
                    model.addAttribute("user", userInfo);
                    model.addAttribute("thisUserId", userInfo.getId());
                    model.addAttribute("incomingFriendRequests", Constants.EMPTY_LIST);
                    model.addAttribute("isCurrentUserFriend", false);
                    model.addAttribute("isFriendshipRequestPending", false);
                    model.addAttribute("isItYourOwnAcc", false);
                    model.addAttribute("feed", postService.getAllPostFeed(userInfo.getId()));

                    model.addAttribute("likeService", likeService);
                    model.addAttribute("isThereNoUserPicture", isThereNoPicture(userInfo.getId()));
                    return "user-profile";
                }
            } else {
                if (isUserLogged(principal)) {
                    model.addAttribute("users", userInfoService.getAll());
                } else {
                    model.addAttribute("users", userInfoService.getAllPublicVisibleUsers());
                }
            }
        } catch (EntityNotFoundException e) {
            return "redirect:/";
        }
        return "users-list";

    }

    @GetMapping("/dudes/{id}")
    public String showUserConnections(@PathVariable UUID id, Model model) {
        model.addAttribute("users", userInfoService.getFriendsOfUser(id));
        return "users-list";
    }

    @GetMapping("/{id}")
    public String showUser(@PathVariable UUID id, @NotNull Model model, Principal principal) {
        model.addAttribute("likeService", likeService);
        model.addAttribute("isThereNoUserPicture", isThereNoPicture(id));
        if (isUserLogged(principal)) {
            UserInfo loggedUser = userInfoService.getByUsername(principal.getName());
            model.addAttribute("loggedUser", loggedUser);
            boolean isItYourOwnAcc = id.equals(loggedUser.getId());
            model.addAttribute("isItYourOwnAcc", isItYourOwnAcc);
            model.addAttribute("user", userInfoService.getById(id));
            model.addAttribute("feed", postService.getAllPostFeed(id));
            if (isItYourOwnAcc) {
                model.addAttribute("incomingFriendRequests", userInfoService.getFriendRequestsOfUser(id));
            } else {
                int relationshipStatusId = getRelationshipStatusId(id, loggedUser);
                if (relationshipStatusId == Constants.BLOCKED) {
                    return "user-blocked";
                } else {
                    model.addAttribute("incomingFriendRequests", Constants.EMPTY_LIST);
                    model.addAttribute("isCurrentUserFriend", isCurrentUserFriend(id, loggedUser));
                    model.addAttribute("isFriendshipRequestPending", relationshipStatusId == Constants.PENDING);
                }
            }
        } else {
            model.addAttribute("user", userInfoService.getById(id));
            model.addAttribute("incomingFriendRequests", Constants.EMPTY_LIST);
            model.addAttribute("isCurrentUserFriend", false);
            model.addAttribute("isFriendshipRequestPending", false);
            model.addAttribute("isItYourOwnAcc", false);
            model.addAttribute("feed", postService.getAllPostFeed(id));
        }
        return "user-profile";
    }

    @GetMapping("/update-form/{id}")
    public String showUpdateUserBasicInfoForm(@PathVariable UUID id, Model model) {
        UserInfoDTO userInfoDto = new UserInfoDTO();
        mapper.userInfoToUserInfoDto(userInfoService.getById(id), userInfoDto);
        model.addAttribute("userId", id);
        model.addAttribute("userDTO", userInfoDto);
        return "user-update";
    }

    @PostMapping("/update/{id}")
    public String updateUserBasic(@PathVariable UUID id,
                             @ModelAttribute("userDTO") @Valid UserInfoDTO userInfoDto,
                             Model model, Principal principal) {
//        UserInfo newUser = mapper.mergeUserInfos(userInfoService.getById(id), userInfoDto);
        userInfoService.updateBasicUserInfo(id, userInfoDto, userInfoService.getByUsername(principal.getName()));
        getYourOwnAccView(id, model, principal);
        return "user-profile";
    }

    @GetMapping("/details/update-form/{id}")
    public String showUpdateUserDetailsInfoForm(@PathVariable UUID id, Model model) {
        UserInfoDetailsDTO userInfoDetailsDTO = new UserInfoDetailsDTO();
        UserInfo userInfo = userInfoService.getById(id);
        userInfoDetailsDTO.setDaysNotGoingOut(userInfo.getDaysNotGoingOut());
        userInfoDetailsDTO.setJob(userInfo.getJob());
        userInfoDetailsDTO.setGenderId(userInfo.getGender().getGenderId());
        userInfoDetailsDTO.setProfileVisibilityId(userInfo.getProfileVisibility().getId());
        model.addAttribute("genders", genderService.getAll());
        model.addAttribute("visibilities", profileVisibilityService.getAll());
        model.addAttribute("userId", id);
        model.addAttribute("userInfoDetailsDTO", userInfoDetailsDTO);
        return "user-details-update";
    }

    @PostMapping("/details/update/{id}")
    public String updateUserDetails(@PathVariable UUID id,
                             @ModelAttribute("userInfoDetailsDTO") @Valid UserInfoDetailsDTO userInfoDetailsDTO,
                             Model model, Principal principal) {
        userInfoDetailsDTO.setBirthday(Date.valueOf(LocalDate.now()));
        userInfoService.updateUserInfoDetails(id, userInfoDetailsDTO, userInfoService.getByUsername(principal.getName()));
        getYourOwnAccView(id, model, principal);
        return "user-profile";
    }

    @PostMapping("/{id}")
    public String delete(@PathVariable UUID id, Principal principal, Model model) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            userInfoService.delete(id, user);
            return "redirect:/";
        } catch (ResponseStatusException e) {
            model.addAttribute("error", e.getMessage());
            return "index";
        }
    }

    @DeleteMapping("/admin/delete/{id}")
    public String deleteByAdmin(@PathVariable UUID id,
                                Principal principal,
                                Model model) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            userInfoService.delete(id, user);
            return "redirect:/";
        } catch (InvalidUserOperation | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "index";
        }
    }

    private boolean isUserLogged(Principal principal) {
        return principal != null;
    }

    private boolean isThereNoPicture(@PathVariable UUID id) {
        return userInfoService.getById(id).getPicture() == null;
    }

    private boolean cannotBeFoundByEmail(@RequestParam(required = false) String names) {
        return postService.getAllPostFeed(userInfoService.getByEmail(names).getId()) == null;
    }

    private boolean isCurrentUserFriend(@PathVariable UUID id, UserInfo loggedUser) {
        return (relationshipService.getRelationshipStatusId(id, loggedUser.getId()) == Constants.FRIEND)
                || (relationshipService.getRelationshipStatusId(loggedUser.getId(), id) == Constants.FRIEND);
    }

    private int getRelationshipStatusId(@PathVariable UUID id, UserInfo loggedUser) {
        return relationshipService.getRelationshipStatusId(id, loggedUser.getId());
    }

    private void getYourOwnAccView(@PathVariable UUID id, Model model, Principal principal) {
        model.addAttribute("likeService", likeService);
        model.addAttribute("isThereNoUserPicture", isThereNoPicture(id));
        UserInfo loggedUser = userInfoService.getByUsername(principal.getName());
        model.addAttribute("loggedUser", loggedUser);
        boolean isItYourOwnAcc = id.equals(loggedUser.getId());
        model.addAttribute("isItYourOwnAcc", isItYourOwnAcc);
        model.addAttribute("user", userInfoService.getById(id));
        model.addAttribute("feed", postService.getAllPostFeed(id));
        model.addAttribute("incomingFriendRequests", userInfoService.getFriendRequestsOfUser(id));
    }
}
