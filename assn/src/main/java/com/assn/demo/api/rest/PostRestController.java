package com.assn.demo.api.rest;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.InconformityException;
import com.assn.demo.models.Post;
import com.assn.demo.models.PostDTO;
import com.assn.demo.models.UserInfo;
import com.assn.demo.services.PostService;
import com.assn.demo.services.UserInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    private PostService postService;
    private UserInfoService userInfoService;

    @Autowired
    public PostRestController(PostService postService, UserInfoService userInfoService) {
        this.postService = postService;
        this.userInfoService = userInfoService;
    }

    @ApiOperation("Get a post by id")
    @GetMapping("/{id}")
    public Post getPostById(@Valid @PathVariable(name = "id") UUID id) {
        try {
            return postService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all posts created by the user")
    @GetMapping("/user/{userId}")
    public List<Post> getAllPostsByUser(@Valid @PathVariable(name = "userId") UUID userId) {
        try {
            return postService.getAllPostsByUser(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all posts where creator is visible for everyone")
    @GetMapping("/all/public")
    public List<Post> getAllPublicVisibleUsersPosts() {
        return postService.getAllPublicVisibleUsersPosts();
    }

    @ApiOperation("Get all posts")
    @GetMapping
    public List<Post> getAllPosts() {
        return postService.getAll();
    }

    @ApiOperation("Get all posts sorted by date in ascending order")
    @GetMapping("/date/asc")
    public List<Post> getAllPostsSortedByDateAsc() {
        return postService.showPostsSortedByDateAsc();
    }

    @ApiOperation("Get all posts sorted by date in descending order")
    @GetMapping("/date/desc")
    public List<Post> getAllPostsSortedByDateDesc() {
        return postService.showPostsSortedByDateDesc();
    }

    @ApiOperation("Get all friend's posts of a user")
    @GetMapping("/{userId}/friends/posts")
    public List<Post> getAllFriendsPostsByUser(@Valid @PathVariable(name = "userId") UUID userId) {
        try {
            return postService.getAllPostFeed(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all friend's posts of a user and paginate returned data")
    @GetMapping("/{userId}/friends/posts/paged")
    public Page<Post> getAllFriendsPostsByUserPaged(@Valid @PathVariable(name = "userId") UUID userId,
                                                    @PageableDefault(value = 10) Pageable pageable) {
        try {
            return postService.getAllPostFeedPaginated(userId, pageable);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Create a post")
    @PostMapping
    public Post add(@Valid @RequestBody PostDTO postDTO, Principal principal) {
        try {
            UserInfo createdBy = userInfoService.getByUsername(principal.getName());
            return postService.create(postDTO, createdBy);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Update a post")
    @PutMapping("/{id}")
    public Post update(@PathVariable(name = "id") UUID postId,
                       @RequestBody @Valid PostDTO postDTO,
                       Principal principal) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            return postService.update(postId, postDTO, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (InconformityException e) {
            throw new ResponseStatusException(BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation("Delete a post")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id, Principal principal) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            postService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (InconformityException e) {
            throw new ResponseStatusException(BAD_REQUEST, e.getMessage());
        }
    }
}
