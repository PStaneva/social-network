package com.assn.demo.api.rest;

import com.assn.demo.exeptions.DuplicateEntityException;
import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.PasswordMismatchException;
import com.assn.demo.exeptions.RelationshipConflictException;
import com.assn.demo.models.*;
import com.assn.demo.services.UserInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.time.DateTimeException;
import java.util.List;
import java.util.UUID;

import static com.assn.demo.utils.ValidationMessages.CHANGE_PASSWORD_SUCCESSFULLY;
import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private UserInfoService userInfoService;
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    public UserRestController(UserInfoService userInfoService,
                              ApplicationEventPublisher eventPublisher) {
        this.userInfoService = userInfoService;
        this.eventPublisher = eventPublisher;
    }

    @ApiOperation("Get a user by id")
    @GetMapping("/{id}")
    public UserInfo getUserById(@Valid @PathVariable(name = "id") UUID id) {
        try {
            return userInfoService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all friends of a user")
    @GetMapping("/{userId}/friends")
    public List<UserInfo> getFriendsOfUser(@Valid @PathVariable(name = "userId") UUID userId) {
        try {
            return userInfoService.getFriendsOfUser(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all friend requests of a user")
    @GetMapping("/{userId}/friend-requests")
    public List<UserInfo> getFriendRequestsOfUser(@Valid @PathVariable(name = "userId") UUID userId) {
        try {
            return userInfoService.getFriendRequestsOfUser(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all blocked users of a user")
    @GetMapping("/{userId}/list-of-blocks")
    public List<UserInfo> getBlockListOfUser(@Valid @PathVariable(name = "userId") UUID userId) {
        try {
            return userInfoService.getBlockList(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get a user by username")
    @GetMapping("/username/{username}")
    public UserInfo getUserByUsername(@Valid @PathVariable(name = "username") String username) {
        try {
            return userInfoService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get a user by email")
    @GetMapping("/email/{email}")
    public UserInfo getUserByEmail(@Valid @PathVariable(name = "email") String email) {
        try {
            return userInfoService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all users")
    @GetMapping
    public List<UserInfo> getAllUsers() {
        return userInfoService.getAll();
    }

    @ApiOperation("Get a user by first and last name")
    @GetMapping("/{firstName}/{lastName}")
    public List<UserInfo> getAllUsersByFullName(@Valid @PathVariable(name = "firstName") String firstName,
                                                @Valid @PathVariable(name = "lastName") String lastName) {

        return userInfoService.getAllUsersByFullName(firstName, lastName);
    }

    @ApiOperation("Get all users whose profile is visible for everyone")
    @GetMapping("/all/public")
    public List<UserInfo> getAllPublicVisibleUsers() {
        return userInfoService.getAllPublicVisibleUsers();
    }

    @ApiOperation("Get information about all users depending on their profile visibility")
    @GetMapping("/{userId}/information")
    public UserVisibilityDTO getUserInfoByVisibility(@Valid @PathVariable(name = "userId") UUID userId,
                                                     Principal principal) {
        try {
            UserInfo sender = userInfoService.getByUsername(principal.getName());
            return userInfoService.getUserInfoByVisibility(userId, sender);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Register a user")
    @PostMapping
    public UserInfo register(@Valid @RequestBody UserRegistrationDTORest dto, HttpServletRequest request) {
        try {
            UserInfo user = userInfoService.create(dto);
            String appURL = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, appURL));
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(CONFLICT, e.getMessage());
        }
    }

    @ApiOperation("Send request to user")
    @PostMapping("/friend-request/{receiverId}")
    public Relationship requestFriendship(@PathVariable UUID receiverId,
                                          Principal principal) {
        try {
            UserInfo sender = userInfoService.getByUsername(principal.getName());
            return userInfoService.requestFriendship(sender.getId(), receiverId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(CONFLICT, e.getMessage());
        }
    }

    @ApiOperation("Confirm email")
    @PutMapping("/email/confirmation")
    public UserInfo emailConfirmationByRegistration(@RequestParam("token") String token) {
        try {
            return userInfoService.emailConfirmationByRegistration(token);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation("Accept a request")
    @PutMapping("/accept/friend-request-of-user/{userId}")
    public Relationship acceptFriendship(@PathVariable UUID userId,
                                         Principal principal) {
        try {
            UserInfo receiver = userInfoService.getByUsername(principal.getName());
            return userInfoService.acceptFriendship(userId, receiver);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (RelationshipConflictException e) {
            throw new ResponseStatusException(CONFLICT, e.getMessage());
        }
    }

    @ApiOperation("Block a friend")
    @PutMapping("/block/friend/{userToBlockId}")
    public void blockFriend(@PathVariable UUID userToBlockId,
                            Principal principal) {
        try {
            UserInfo sender = userInfoService.getByUsername(principal.getName());
            userInfoService.blockFriend(userToBlockId, sender);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (RelationshipConflictException e) {
            throw new ResponseStatusException(CONFLICT, e.getMessage());
        }
    }

    @ApiOperation("Update basic user information")
    @PutMapping("/{id}")
    public UserInfo updateBasicUserInfo(@PathVariable(name = "id") UUID userId,
                                        @RequestBody @Valid UserInfoDTO dto,
                                        Principal principal) {
        try {
            UserInfo userToBeAuthenticated = userInfoService.getByUsername(principal.getName());
            return userInfoService.updateBasicUserInfo(userId, dto, userToBeAuthenticated);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException m) {
            throw new ResponseStatusException(CONFLICT, m.getMessage());
        }
    }

    @ApiOperation("Update user details")
    @PutMapping("/details/{id}")
    public UserInfo updateUserInfoDetails(@PathVariable(name = "id") UUID userId,
                                          @RequestBody @Valid UserInfoDetailsDTO dto,
                                          Principal principal) {
        try {
            UserInfo userToBeAuthenticated = userInfoService.getByUsername(principal.getName());
            return userInfoService.updateUserInfoDetails(userId, dto, userToBeAuthenticated);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (DateTimeException e) {
            throw new ResponseStatusException(BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation("Change user's password by user id")
    @PutMapping("/{id}/password")
    public String changePassword(@PathVariable(name = "id") UUID userId,
                                 @Valid @RequestBody PasswordChangeDTO passwordDTO,
                                 Principal principal) {
        try {
            UserInfo userToBeAuthenticated = userInfoService.getByUsername(principal.getName());
            UserInfo user = userInfoService.getById(userId);
            userInfoService.changePassword(user, passwordDTO, userToBeAuthenticated);
            return CHANGE_PASSWORD_SUCCESSFULLY;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (PasswordMismatchException e) {
            throw new ResponseStatusException(CONFLICT, e.getMessage());
        }
    }

    @ApiOperation("Delete a user")
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id, Principal principal) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            userInfoService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Remove a user from friends list")
    @DeleteMapping("/remove/friend/{userId}")
    public void unfriendUser(@PathVariable UUID userId,
                             Principal principal) {
        try {
            UserInfo receiver = userInfoService.getByUsername(principal.getName());
            userInfoService.unfriendUser(userId, receiver);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Decline a friend request")
    @DeleteMapping("/decline/friend-request-of-user/{userId}")
    public void declineFriendship(@PathVariable UUID userId,
                                  Principal principal) {
        try {
            UserInfo receiver = userInfoService.getByUsername(principal.getName());
            userInfoService.declineFriendship(userId, receiver);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Unblock a user (remove user from blocked list)")
    @DeleteMapping("/unblock/friend/{userToUnblockId}")
    public void unblockFriend(@PathVariable UUID userToUnblockId,
                              Principal principal) {
        try {
            UserInfo sender = userInfoService.getByUsername(principal.getName());
            userInfoService.unblockFriend(userToUnblockId, sender);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (RelationshipConflictException e) {
            throw new ResponseStatusException(CONFLICT, e.getMessage());
        }
    }
}