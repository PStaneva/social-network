package com.assn.demo.api.rest;

import com.assn.demo.exeptions.DuplicateEntityException;
import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.UserInfo;
import com.assn.demo.services.LikeService;
import com.assn.demo.services.UserInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api/likes")
public class LikeRestController {

    private LikeService likeService;
    private UserInfoService userInfoService;

    @Autowired
    public LikeRestController(LikeService likeService, UserInfoService userInfoService) {
        this.likeService = likeService;
        this.userInfoService = userInfoService;
    }

    @ApiOperation("Get all users that are liked the post")
    @GetMapping("/post/{postId}")
    public List<UserInfo> getAllUsersLikedPost(@PathVariable UUID postId) {
        try {
            return likeService.getAllUsersLikedPost(postId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Like a post")
    @PostMapping("/post/{postId}")
    public void likePost(Principal principal, @PathVariable UUID postId) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            likeService.likePost(postId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(CONFLICT, e.getMessage());
        }
    }

    @ApiOperation("Unlike a post")
    @DeleteMapping("/post/{postId}")
    public void unlikePost(Principal principal, @PathVariable UUID postId) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            likeService.unlikePost(postId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }
}
