package com.assn.demo.api;

import com.assn.demo.models.OnRegistrationCompleteEvent;
import com.assn.demo.models.UserInfo;
import com.assn.demo.services.EmailService;
import com.assn.demo.services.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import static java.util.UUID.randomUUID;

@Component
public class RegistrationListener implements
        ApplicationListener<OnRegistrationCompleteEvent> {

    private UserInfoService userInfoService;
    private EmailService emailService;

    @Autowired
    public RegistrationListener(UserInfoService userInfoService,
                                EmailService emailService) {
        this.userInfoService = userInfoService;
        this.emailService = emailService;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        UserInfo user = event.getUser();
        String token = randomUUID().toString();
        userInfoService.createVerificationToken(user, token);

        String recipientEmail = user.getEmail();
        String subject = "Registration Confirmation:\n";
        String confirmationURL = event.getAppUrl() +
                "/registration/confirmed?token=" + token;
        String text = "Thank you for registering! Please click on the below link to activate your account.\n";

        emailService.sendEmailViaMailTrap(recipientEmail, subject, text, confirmationURL);
    }
}
