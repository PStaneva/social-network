package com.assn.demo.api.mvc;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.CommentDTO;
import com.assn.demo.models.Post;
import com.assn.demo.models.UserInfo;
import com.assn.demo.services.CommentService;
import com.assn.demo.services.LikeService;
import com.assn.demo.services.PostService;
import com.assn.demo.services.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;
import static com.assn.demo.api.mvc.PostController.getPostView;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Controller
@RequestMapping
public class CommentController {

    private UserInfoService userInfoService;
    private CommentService commentService;
    private LikeService likeService;
    private PostService postService;

    @Autowired
    public CommentController(UserInfoService userInfoService,
                             CommentService commentService,
                             LikeService likeService,
                             PostService postService) {
        this.userInfoService = userInfoService;
        this.commentService = commentService;
        this.likeService = likeService;
        this.postService = postService;
    }

    @GetMapping("/comment/{id}")
    public String getCommentById(@Valid @PathVariable UUID id, Model model) {
        model.addAttribute("comment", commentService.getById(id));
        return "comment-read";
    }

    @PostMapping("/comment/{id}")
    public String deleteCommentById(@Valid @PathVariable UUID id, Principal principal, Model model) {
        UUID postId = commentService.getById(id).getPost().getId();
        commentService.delete(id, userInfoService.getByUsername(principal.getName()));
        getPostView(postId, principal, model, postService, commentService, userInfoService, likeService);
        return "post-read";
    }

    @GetMapping("/comment/create-form/{postId}")
    public String showCreateCommentForm(@PathVariable UUID postId, Model model) {
        model.addAttribute("commentDTO", new CommentDTO());
        model.addAttribute("id", postId);
        return "comment-create";
    }

    @PostMapping("/comment/create/{postId}")
    public String createComment(@PathVariable("postId") UUID postId,
                                Model model,
                                @ModelAttribute("commentDTO") @Valid CommentDTO commentDTO,
                                Principal principal) {
        UserInfo userInfo = userInfoService.getByUsername(principal.getName());
        commentService.create(postId, commentDTO, userInfo);
        try {
            Post post = postService.getById(postId);
            model.addAttribute("isThereNoPicture", post.getPicture() == null);
            model.addAttribute("post", post);
            model.addAttribute("comments", commentService.getAllCommentsByPost(postId));
            model.addAttribute("postService", postService);
            model.addAttribute("isUserLikedIt", likeService.isPostLikedByUser(postId, userInfo));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
        return "post-read";
    }

    @ModelAttribute("users")
    public List<UserInfo> populateUsers() {
        return userInfoService.getAll();
    }

    @ModelAttribute("userService")
    public UserInfoService populateUserService() {
        return userInfoService;
    }
}
