package com.assn.demo.api.rest;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.ProfileVisibility;
import com.assn.demo.services.ProfileVisibilityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/profile/visibility")
public class ProfileVisibilityRestController {

    private ProfileVisibilityService profileVisibilityService;

    @Autowired
    public ProfileVisibilityRestController(ProfileVisibilityService profileVisibilityService) {
        this.profileVisibilityService = profileVisibilityService;
    }

    @ApiOperation("Get a profile visibility type by id")
    @GetMapping("/{id}")
    public ProfileVisibility getProfileVisibilityById(@Valid @PathVariable(name = "id") UUID id) {
        try {
            return profileVisibilityService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get a profile visibility by type")
    @GetMapping("/types/{type}")
    public ProfileVisibility getProfileVisibilityType(@Valid @PathVariable String type) {
        try {
            return profileVisibilityService.getByType(type);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all profile visibility types")
    @GetMapping
    public List<ProfileVisibility> getAllProfileVisibilityTypes() {
        return profileVisibilityService.getAll();
    }
}
