package com.assn.demo.api.mvc;

import com.assn.demo.exeptions.DuplicateEntityException;
import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Mapper;
import com.assn.demo.models.OnRegistrationCompleteEvent;
import com.assn.demo.models.UserInfo;
import com.assn.demo.models.UserInfoDTOMvc;
import com.assn.demo.services.UserInfoService;
import com.assn.demo.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    private UserInfoService userInfoService;
    private ApplicationEventPublisher eventPublisher;
    private Mapper mapper;

    @Autowired
    public RegistrationController(UserInfoService userInfoService, ApplicationEventPublisher eventPublisher, Mapper mapper) {
        this.userInfoService = userInfoService;
        this.eventPublisher = eventPublisher;
        this.mapper = mapper;
    }

    @GetMapping()
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping()
    public String registerUserAccount(@ModelAttribute("userRegistrationDto") @Valid UserInfoDTOMvc userDto,
                                      HttpServletRequest request, BindingResult bindingResult) {
        try {
            UserInfo user = userInfoService.create(mapper.UserRegDtoMvcToRest(userDto));
            String appURL = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, appURL));
            return "redirect:/index";
        } catch (DuplicateEntityException ex) {
            bindingResult.rejectValue("email",
                    null,
                    Constants.THERE_IS_AN_ACCOUNT_REGISTERED_WITH_THAT_EMAIL);
            return "registration";
        }
    }

    @GetMapping("/confirmed")
    public String showRegisterConfirmation(@Valid @RequestParam("token") String token) {
        try {
            userInfoService.emailConfirmationByRegistration(token);
            return "registration-confirmed";
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(BAD_REQUEST, ex.getMessage());
        }
    }

    @ModelAttribute("userRegistrationDto")
    public UserInfoDTOMvc userRegistrationDto() {
        return new UserInfoDTOMvc();
    }
}
