package com.assn.demo.api.mvc;

import com.assn.demo.services.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.security.Principal;
import java.util.UUID;

@Controller
@RequestMapping("/friendship")
public class FriendshipController {

    UserInfoService userInfoService;

    @Autowired
    public FriendshipController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @PostMapping("/request/{soonToBeFriendId}")
    public String requestFriendship(@PathVariable UUID soonToBeFriendId, Principal principal) {
        userInfoService.requestFriendship(userInfoService.getByUsername(principal.getName()).getId(), soonToBeFriendId);
        return "friend-request-sent";
    }

    @PostMapping("/accept/{soonToBeFriendId}")
    public String acceptFriendship(@PathVariable UUID soonToBeFriendId, Principal principal) {
        userInfoService.acceptFriendship(soonToBeFriendId, userInfoService.getByUsername(principal.getName()));
        return "friend-request-accept";
    }

    @PostMapping("/decline/{notToBeFriendId}")
    public String declineFriendship(@PathVariable UUID notToBeFriendId, Principal principal) {
        userInfoService.declineFriendship(notToBeFriendId, userInfoService.getByUsername(principal.getName()));
        return "friend-request-decline";
    }

    @PostMapping("/end/{userToUnfriendId}")
    public String endFriendship(@PathVariable UUID userToUnfriendId, Principal principal) {
        userInfoService.unfriendUser(userToUnfriendId, userInfoService.getByUsername(principal.getName()));
        return "friendship-end";
    }

    @PostMapping("/blacklist/{userToBlacklistId}")
    public String blackLIstUser(@PathVariable UUID userToBlacklistId, Principal principal) {
        userInfoService.blockFriend(userToBlacklistId, userInfoService.getByUsername(principal.getName()));
        return "user-blacklist";
    }

    @ModelAttribute("userService")
    public UserInfoService populateUserService() {
        return userInfoService;
    }
}
