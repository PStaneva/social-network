package com.assn.demo.api.rest;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.StatusType;
import com.assn.demo.services.StatusTypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/status")
public class StatusTypeRestController {

    private StatusTypeService statusTypeService;

    @Autowired
    public StatusTypeRestController(StatusTypeService statusTypeService) {
        this.statusTypeService = statusTypeService;
    }

    @ApiOperation("Get a status type by id")
    @GetMapping("/{id}")
    public StatusType getStatusTypeById(@Valid @PathVariable(name = "id") Integer id) {
        try {
            return statusTypeService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get a status by type")
    @GetMapping("/types/{statusType}")
    public StatusType getStatusType(@Valid @PathVariable(name = "statusType") String statusType) {
        try {
            return statusTypeService.getByType(statusType);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all status types")
    @GetMapping
    public List<StatusType> getAllStatusTypes() {
        return statusTypeService.getAll();
    }
}
