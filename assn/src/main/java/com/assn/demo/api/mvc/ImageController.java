package com.assn.demo.api.mvc;

import com.assn.demo.models.Post;
import com.assn.demo.models.PostDTO;
import com.assn.demo.models.UserInfo;
import com.assn.demo.services.PostService;
import com.assn.demo.services.UserInfoService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Base64;
import java.util.UUID;

@Controller
public class ImageController {

    private PostService postService;
    private UserInfoService userInfoService;

    @Autowired
    public ImageController(PostService postService, UserInfoService userInfoService) {
        this.postService = postService;
        this.userInfoService = userInfoService;
    }

    @GetMapping("post/image/upload/{id}")
    public String showPostUploadForm(@PathVariable UUID id, Model model) {
        model.addAttribute("post", postService.getById(id));
        return "upload-post-picture";
    }

    @PostMapping("post/image/upload/{id}")
    public String savePostImage(@PathVariable UUID id, @RequestParam("imagefile") MultipartFile file,
                                      Principal principal) throws IOException {
        UserInfo user = userInfoService.getByUsername(principal.getName());
        PostDTO dto = new PostDTO();
        dto.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
        postService.update(id, dto, user);
        return "redirect:/";
    }

    @GetMapping("post/image/{id}")
    public void renderPostImageFromDB(@PathVariable UUID id, HttpServletResponse response) throws IOException {
        Post post = postService.getById(id);
        if (post.getPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(post.getPicture()));
            IOUtils.copy(is, response.getOutputStream());
        }
    }

    @GetMapping("user/image")
    public String showUserUploadForm(Model model, Principal principal) {
        model.addAttribute("user", userInfoService.getByUsername(principal.getName()));
        return "upload-profile-picture";
    }

    @PostMapping("user/image")
    public String saveUserImage(@RequestParam("imagefile") MultipartFile file, Principal principal) throws IOException {
        userInfoService.updatePicture(userInfoService.getByUsername(principal.getName()).getId(),
                Base64.getEncoder().encodeToString(file.getBytes()));
        return "redirect:/";
    }

    @GetMapping("user/image/{id}")
    public void renderUserImageFromDB(@PathVariable UUID id, HttpServletResponse response) throws IOException {
        UserInfo user = userInfoService.getById(id);
        if (user.getPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(user.getPicture()));
            IOUtils.copy(is, response.getOutputStream());
        }
    }
}
