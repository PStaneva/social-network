package com.assn.demo.api.rest;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Relationship;
import com.assn.demo.services.RelationshipService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/relationships")
public class RelationshipRestController {

    private RelationshipService relationshipService;

    @Autowired
    public RelationshipRestController(RelationshipService relationshipService) {
        this.relationshipService = relationshipService;
    }

    @ApiOperation("Get a connection by id")
    @GetMapping("/{id}")
    public Relationship getRelationshipById(@Valid @PathVariable(name = "id") UUID id) {
        try {
            return relationshipService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all connections")
    @GetMapping
    public List<Relationship> getAllRelationships() {
        return relationshipService.getAll();
    }
}
