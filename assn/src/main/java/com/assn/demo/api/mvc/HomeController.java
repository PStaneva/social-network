package com.assn.demo.api.mvc;

import com.assn.demo.services.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    private UserInfoService userInfoService;

    @Autowired
    public HomeController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @GetMapping("/")
    public String index(){
        return "index";
    }

    @GetMapping("/index")
    public String indexFull(){
        return "index";
    }

    @GetMapping("/under-construction")
    public String underConstruction(){
        return "under-construction";
    }

    @GetMapping("/sw")
    public String swagger() {
        return "redirect:/swagger-ui.html";
    }

    @GetMapping("/about")
    public String about(){
        return "about";
    }

    @ModelAttribute("userService")
    public UserInfoService populateUsers() {
        return userInfoService;}
}
