package com.assn.demo.api.rest;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Gender;
import com.assn.demo.services.GenderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/genders")
public class GenderRestController {

    private GenderService genderService;

    @Autowired
    public GenderRestController(GenderService genderService) {
        this.genderService = genderService;
    }

    @ApiOperation("Get gender by id")
    @GetMapping("/{id}")
    public Gender getGenderById(@Valid @PathVariable(name = "id") UUID id) {
        try {
            return genderService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get gender by type")
    @GetMapping("/types/{type}")
    public Gender getGenderType(@Valid @PathVariable String type) {
        try {
            return genderService.getByType(type);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all gender types")
    @GetMapping
    public List<Gender> getAllGenderTypes() {
        return genderService.getAll();
    }
}
