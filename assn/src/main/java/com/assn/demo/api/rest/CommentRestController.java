package com.assn.demo.api.rest;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.InconformityException;
import com.assn.demo.models.Comment;
import com.assn.demo.models.CommentDTO;
import com.assn.demo.models.UserInfo;
import com.assn.demo.services.CommentService;
import com.assn.demo.services.UserInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {

    private CommentService commentService;
    private UserInfoService userInfoService;

    @Autowired
    public CommentRestController(CommentService commentService, UserInfoService userInfoService) {
        this.commentService = commentService;
        this.userInfoService = userInfoService;
    }

    @ApiOperation("Get a comment by id")
    @GetMapping("/{id}")
    public Comment getCommentById(@Valid @PathVariable(name = "id") UUID id) {
        try {
            return commentService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all comments on a post")
    @GetMapping("/post/{postId}")
    public List<Comment> getAllCommentsByPost(@Valid @PathVariable UUID postId) {
        try {
            return commentService.getAllCommentsByPost(postId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get all comments")
    @GetMapping
    public List<Comment> getAllComments() {
        return commentService.getAllSortedChronologically();
    }

    @ApiOperation("Comment a post")
    @PostMapping("/post/{postId}")
    public Comment addCommentToPost(Principal principal,
                                    @PathVariable UUID postId,
                                    @Valid @RequestBody CommentDTO commentDTO) {
        try {
            UserInfo createdBy = userInfoService.getByUsername(principal.getName());
            return commentService.create(postId, commentDTO, createdBy);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Add response to a comment on a post")
    @PostMapping("/post/comment/{commentId}/response")
    public Comment addResponseToComment(Principal principal,
                                        @PathVariable UUID commentId,
                                        @Valid @RequestBody CommentDTO commentDTO) {
        try {
            UserInfo createdBy = userInfoService.getByUsername(principal.getName());
            return commentService.addResponseToComment(commentId, commentDTO, createdBy);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Update a post")
    @PutMapping("/{id}")
    public Comment update(@PathVariable(name = "id") UUID commentId,
                          @RequestBody @Valid CommentDTO commentDTO,
                          Principal principal) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            return commentService.update(commentId, commentDTO, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (InconformityException e) {
            throw new ResponseStatusException(BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation("Delete a comment from post")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id, Principal principal) {
        try {
            UserInfo user = userInfoService.getByUsername(principal.getName());
            commentService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(NOT_FOUND, e.getMessage());
        } catch (InconformityException e) {
            throw new ResponseStatusException(BAD_REQUEST, e.getMessage());
        }
    }
}
