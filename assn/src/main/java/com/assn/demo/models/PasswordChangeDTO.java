package com.assn.demo.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.assn.demo.utils.ValidationMessages.*;

@Data
@NoArgsConstructor
public class PasswordChangeDTO {

    @NotBlank
    @NotNull
    private String currentPassword;

    @NotBlank
    @NotNull
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH, message = USER_PASSWORD_ERROR_LENGTH_MESSAGE)
    private String newPassword;

    @NotBlank
    @NotNull
    private String confirmPassword;
}
