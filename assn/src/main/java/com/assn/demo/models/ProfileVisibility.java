package com.assn.demo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.UUID;

import static com.assn.demo.utils.ValidationMessages.*;

@Entity
@Table(name = "profile_visibility")
@Getter
@Setter
@NoArgsConstructor
public class ProfileVisibility {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "id", columnDefinition = "BINARY(16)", unique = true)
    private UUID id;

    @Column(name = "type")
    @Size(min = PROFILE_PRIVACY_TYPE_MIN_LENGTH, max = PROFILE_PRIVACY_TYPE_MAX_LENGTH,
            message = PROFILE_PRIVACY_TYPE_ERROR_MESSAGE)
    private String type;
}
