package com.assn.demo.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import static com.assn.demo.utils.ValidationMessages.*;

@Data
@NoArgsConstructor
public class UserInfoDTO {

    @NotBlank
    @NotNull
    @Email(message = EMAIL_ERROR_MESSAGE)
    private String email;

    @NotBlank
    @NotNull
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH, message = USERNAME_ERROR_LENGTH_MESSAGE)
    private String username;

    @NotBlank
    @NotNull
    @Size(min = FIRST_NAME_MIN_LENGTH, max = FIRST_NAME_MAX_LENGTH, message = USER_FIRST_NAME_ERROR_LENGTH_MESSAGE)
    private String firstName;

    @NotBlank
    @NotNull
    @Size(min = LAST_NAME_MIN_LENGTH, max = LAST_NAME_MAX_LENGTH, message = USER_LAST_NAME_ERROR_LENGTH_MESSAGE)
    private String lastName;
}
