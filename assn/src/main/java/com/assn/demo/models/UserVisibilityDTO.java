package com.assn.demo.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserVisibilityDTO {

    private String email;

    private String username;

    private String firstName;

    private String lastName;

    private String job;

    private Long daysNotGoingOut;

    private Gender gender;
}
