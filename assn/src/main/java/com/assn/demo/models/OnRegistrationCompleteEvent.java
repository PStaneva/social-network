package com.assn.demo.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private String appUrl;
    private UserInfo user;

    public OnRegistrationCompleteEvent(UserInfo user, String appUrl) {
        super(user);

        this.user = user;
        this.appUrl = appUrl;
    }
}
