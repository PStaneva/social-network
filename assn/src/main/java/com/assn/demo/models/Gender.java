package com.assn.demo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.UUID;

import static com.assn.demo.utils.ValidationMessages.*;

@Entity
@Table(name = "gender")
@Getter
@Setter
@NoArgsConstructor
public class Gender {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "gender_id", columnDefinition = "BINARY(16)", unique = true)
    private UUID genderId;

    @Column(name = "type")
    @Size(min = GENDER_TYPE_MIN_LENGTH, max = GENDER_TYPE_MAX_LENGTH, message = GENDER_TYPE_ERROR_MESSAGE)
    private String type;
}
