package com.assn.demo.models;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class Feedback {

    @NotNull
    private String username;

    @NotNull
    @Email
    private String email;

    @NotNull
    @Min(10)
    private String feedback;
}
