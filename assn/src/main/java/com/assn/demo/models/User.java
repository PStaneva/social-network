package com.assn.demo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import static com.assn.demo.utils.ValidationMessages.*;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
public class User {

    @Id
    @Column(name = "username")
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH, message = USERNAME_ERROR_LENGTH_MESSAGE)
    private String username;

    @Column(name = "password")
    @NotBlank
    @NotNull
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH, message = USER_PASSWORD_ERROR_LENGTH_MESSAGE)
    private String password;

    @Transient
    private String passwordConfirmation;

    @Column(name = "enabled")
    private boolean enabled;
}