package com.assn.demo.models;

import com.assn.demo.services.GenderService;
import com.assn.demo.services.ProfileVisibilityService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static java.util.UUID.randomUUID;

@Component
@NoArgsConstructor
public class Mapper {

    private GenderService genderService;
    private ProfileVisibilityService visibilityService;

    @Autowired
    public Mapper(GenderService genderService, ProfileVisibilityService visibilityService) {
        this.genderService = genderService;
        this.visibilityService = visibilityService;
    }

    public Like toLike(Post post, UserInfo user) {
        Like like = new Like();

        like.setId(randomUUID());
        like.setPost(post);
        like.setUserInfo(user);
        like.setLiked(true);

        return like;
    }

    public Comment toComment(CommentDTO commentDTO, Post post, UserInfo createdBy) {
        Comment comment = new Comment();

        comment.setId(randomUUID());
        comment.setText(commentDTO.getText());
        comment.setCreatedAt(LocalDateTime.now());
        comment.setPost(post);
        comment.setCreatedBy(createdBy);

        return comment;
    }

    public Post toPost(PostDTO postDTO, UserInfo createdBy) {
        Post post = new Post();

        post.setId(randomUUID());
        post.setTitle(postDTO.getTitle());
        post.setContent(postDTO.getContent());
        post.setLocation(postDTO.getLocation());
        post.setPicture(postDTO.getPicture());
        post.setCreatedAt(LocalDateTime.now());
        post.setModifiedAt(LocalDateTime.now());
        post.setCreatedBy(createdBy);

        return post;
    }

    public Relationship toRelationship(UserInfo sender, UserInfo receiver, StatusType status) {
        Relationship relationship = new Relationship();

        relationship.setId(randomUUID());
        relationship.setSender(sender);
        relationship.setReceiver(receiver);
        relationship.setStatusType(status);

        return relationship;
    }

    public UserInfo toUserInfo(UserRegistrationDTORest dto) {
        UserInfo user = new UserInfo();

        user.setId(randomUUID());
        user.setUsername(dto.getUsername());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());

        return user;
    }

    public UserVisibilityDTO toUserVisibilityDTOWithRequiredFields(UserInfo user) {
        UserVisibilityDTO userToReturn = new UserVisibilityDTO();

        userToReturn.setUsername(user.getUsername());
        userToReturn.setFirstName(user.getFirstName());
        userToReturn.setLastName(user.getLastName());
        userToReturn.setEmail(user.getEmail());

        return userToReturn;
    }

    public UserVisibilityDTO toUserVisibilityDTOWithAdditionalFields(UserInfo user) {
        UserVisibilityDTO userToReturn = toUserVisibilityDTOWithRequiredFields(user);

        userToReturn.setDaysNotGoingOut(user.getDaysNotGoingOut());
        userToReturn.setJob(user.getJob());
        userToReturn.setGender(user.getGender());

        return userToReturn;
    }

    public UserRegistrationDTORest UserRegDtoMvcToRest(UserInfoDTOMvc dto) {
        UserRegistrationDTORest user = new UserRegistrationDTORest();

        user.setUsername(dto.getUsername());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPassword());

        return user;
    }

    public Comment mergeComments(Comment oldComment, CommentDTO newComment) {
        Comment comment = new Comment();

        comment.setId(oldComment.getId());
        comment.setText(getNotNull(newComment.getText(), oldComment.getText()));
        comment.setCreatedAt(oldComment.getCreatedAt());
        comment.setPost(oldComment.getPost());
        comment.setCreatedBy(oldComment.getCreatedBy());

        return comment;
    }

    public Post mergePosts(Post oldPost, PostDTO newPost) {
        Post post = new Post();

        post.setId(oldPost.getId());
        post.setTitle(getNotNull(newPost.getTitle(), oldPost.getTitle()));
        post.setContent(getNotNull(newPost.getContent(), oldPost.getContent()));
        post.setLocation(getNotNull(newPost.getLocation(), oldPost.getLocation()));
        post.setPicture(newPost.getPicture());
        post.setCreatedAt(oldPost.getCreatedAt());
        post.setModifiedAt(LocalDateTime.now());
        post.setCreatedBy(oldPost.getCreatedBy());
        post.setLikesCount(oldPost.getLikesCount());
        post.setLikes(oldPost.getLikes());
        post.setComments(oldPost.getComments());

        return post;
    }

    public UserInfo mergeUserInfos(UserInfo oldUser, UserInfoDTO newUser) {
        UserInfo user = new UserInfo();

        user.setId(oldUser.getId());
        user.setUsername(getNotNull(newUser.getUsername(), oldUser.getUsername()));
        user.setFirstName(getNotNull(newUser.getFirstName(), oldUser.getFirstName()));
        user.setLastName(getNotNull(newUser.getLastName(), oldUser.getLastName()));
        user.setEmail(getNotNull(newUser.getEmail(), oldUser.getEmail()));
        user.setPicture(oldUser.getPicture());
        user.setJob(oldUser.getJob());
        user.setDaysNotGoingOut(oldUser.getDaysNotGoingOut());
        user.setGender(oldUser.getGender());
        user.setProfileVisibility(oldUser.getProfileVisibility());
        user.setBirthday(oldUser.getBirthday());

        return user;
    }

    public UserInfo mergeUserInfoDetailsDTO(UserInfo oldUser, UserInfoDetailsDTO dto) {
        UserInfo user = new UserInfo();

        user.setId(oldUser.getId());
        user.setUsername(oldUser.getUsername());
        user.setFirstName(oldUser.getFirstName());
        user.setLastName(oldUser.getLastName());
        user.setEmail(oldUser.getEmail());
        user.setPicture(oldUser.getPicture());
        user.setJob(dto.getJob());
        user.setDaysNotGoingOut(dto.getDaysNotGoingOut());
        user.setGender(genderService.getById(dto.getGenderId()));
        user.setProfileVisibility(visibilityService.getById(dto.getProfileVisibilityId()));
        user.setBirthday(dto.getBirthday());

        return user;
    }

    public void userInfoToUserInfoDto(UserInfo userToUpdate, UserInfoDTO userInfoDto) {
        userInfoDto.setUsername(userToUpdate.getUsername());
        userInfoDto.setFirstName(userToUpdate.getFirstName());
        userInfoDto.setLastName(userToUpdate.getLastName());
        userInfoDto.setEmail(userToUpdate.getEmail());
    }

    private <T> T getNotNull(T a, T b) {
        return b != null && a != null && !a.equals(b) ? a : b;
    }
}
