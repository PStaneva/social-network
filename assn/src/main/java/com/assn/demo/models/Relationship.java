package com.assn.demo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "relationships",
        uniqueConstraints = {@UniqueConstraint(columnNames = "sender"),
                @UniqueConstraint(columnNames = "receiver")})
@Getter
@Setter
@NoArgsConstructor
public class Relationship {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "relationship_id", columnDefinition = "BINARY(16)", unique = true)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "sender")
    private UserInfo sender;

    @ManyToOne
    @JoinColumn(name = "receiver")
    private UserInfo receiver;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private StatusType statusType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Relationship that = (Relationship) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sender, that.sender) &&
                Objects.equals(receiver, that.receiver) &&
                Objects.equals(statusType, that.statusType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sender, receiver, statusType);
    }
}

