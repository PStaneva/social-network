package com.assn.demo.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.assn.demo.utils.ValidationMessages.*;

@Data
@NoArgsConstructor
public class PostDTO {

    @NotBlank
    @NotNull
    @Size(min = POST_TITLE_MIN_LENGTH, max = POST_TITLE_MAX_LENGTH, message = POST_TITLE_LENGTH_ERROR_MESSAGE)
    private String title;

    @NotBlank
    @NotNull
    @Size(min = POST_CONTENT_MIN_LENGTH, max = POST_CONTENT_MAX_LENGTH, message = POST_CONTENT_LENGTH_ERROR_MESSAGE)
    private String content;

    @Size(max = POST_LOCATION_MAX_LENGTH, message = POST_LOCATION_LENGTH_ERROR_MESSAGE)
    private String location;

    @Lob
    private String picture;
}
