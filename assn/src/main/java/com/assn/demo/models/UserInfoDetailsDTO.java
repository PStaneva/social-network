package com.assn.demo.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.UUID;
import static com.assn.demo.utils.ValidationMessages.*;

@Data
@NoArgsConstructor
public class UserInfoDetailsDTO {

    private Date birthday;

    @Size(min = JOB_MIN_LENGTH, max = JOB_MAX_LENGTH, message = JOB_ERROR_MESSAGE)
    private String job;

    @Positive
    private Long daysNotGoingOut;

    private UUID genderId;

    private UUID profileVisibilityId;
}
