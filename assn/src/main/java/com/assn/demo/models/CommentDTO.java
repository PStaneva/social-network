package com.assn.demo.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.assn.demo.utils.ValidationMessages.*;

@Data
@NoArgsConstructor
public class CommentDTO {

    @NotNull
    @Size(min = COMMENT_TEXT_MIN_LENGTH, max = COMMENT_TEXT_MAX_LENGTH, message = COMMENT_TEXT_LENGTH_ERROR_MESSAGE)
    private String text;
}
