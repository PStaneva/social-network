package com.assn.demo.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.Size;
import static com.assn.demo.utils.ValidationMessages.*;

@Entity
@Table(name = "status_types")
@Data
@NoArgsConstructor
public class StatusType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "status_id")
    private int statusId;

    @Column(name = "status_type")
    @Size(min = STATUS_TYPE_MIN_LENGTH, max = STATUS_TYPE_MAX_LENGTH, message = STATUS_TYPE_ERROR_MESSAGE)
    private String statusType;
}
