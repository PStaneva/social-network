package com.assn.demo.utils;

import java.util.ArrayList;

public class Constants {
    public static final int PENDING = 1;
    public static final int FRIEND = 2;
    public static final int BLOCKED = 3;
    public static final ArrayList EMPTY_LIST = new ArrayList();
    public static final String THERE_IS_AN_ACCOUNT_REGISTERED_WITH_THAT_EMAIL = "There is an account registered with that email";
}
