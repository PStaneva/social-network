package com.assn.demo.utils;

public class ValidationMessages {

    // USER VALIDATION MESSAGES
    public static final String USERNAME_ERROR_LENGTH_MESSAGE = "Username must be between {min} and {max} characters long.";
    public static final String USER_PASSWORD_ERROR_LENGTH_MESSAGE = "Password must be between {min} and {max} characters long.";
    public static final String USER_FIRST_NAME_ERROR_LENGTH_MESSAGE = "First name must be between {min} and {max} characters long.";
    public static final String USER_LAST_NAME_ERROR_LENGTH_MESSAGE = "Last name must be between {min} and {max} characters long.";
    public static final String EMAIL_ERROR_MESSAGE = "You have entered an invalid email address.";
    public static final String USER_UPDATE_PERMISSION_MESSAGE_FORMAT = "User %s can modify only his own profile.";
    public static final String USER_DELETE_PERMISSION_MESSAGE_FORMAT = "User %s can delete only his own profile.";
    public static final String CURRENT_PASSWORD_MISMATCH_MESSAGE = "Password you entered does not match the current password.";
    public static final String PASSWORD_MISMATCH_MESSAGE = "New password does not match the confirm password.";
    public static final String CHANGE_PASSWORD_SUCCESSFULLY = "Your password has been changed successfully.";
    public static final String STATUS_TYPE_ERROR_MESSAGE = "Status type must be between {min} and {max} characters long.";
    public static final String RELATIONSHIP_DUPLICATE_MESSAGE_FORMAT = "Relationship between %s and %s already exists.";
    public static final String RELATIONSHIP_ERROR_MESSAGE = "You cannot send a friend request to yourself.";
    public static final String CONFLICT_BY_ACCEPT_ERROR_MESSAGE = "You are not allowed to enter your own ID.";
    public static final String CONFLICT_BY_DECLINE_ERROR_MESSAGE = "You are not allowed to enter your own ID.";
    public static final String CONFLICT_BY_REMOVE_FRIEND_ERROR_MESSAGE = "You are not allowed to remove yourself from friend list.";
    public static final String RELATIONSHIP_EXIST_ERROR_MESSAGE_FORMAT = "There is not a friend request from user %s.";
    public static final String FRIEND_NOT_EXIST_ERROR_MESSAGE_FORMAT = "There is not a user %s in your friend list.";
    public static final String BLOCK_YOURSELF_ERROR_MESSAGE = "You cannot block yourself.";
    public static final String BLOCK_NONFRIEND_ERROR_MESSAGE_FORMAT = "You cannot block %s. You are not friends.";
    public static final String UNBLOCK_YOURSELF_ERROR_MESSAGE = "You cannot unblock yourself.";
    public static final String UNBLOCK_ERROR_MESSAGE_FORMAT = "You cannot unblock %s. %s is not in your block list.";
    public static final String JOB_ERROR_MESSAGE = "Job must be between {min} and {max} characters long.";
    public static final String EXPIRED_AUTHENTICATION_ERROR_MESSAGE = "Authentication email confirmation is expired.";

    // USER VALIDATION PARAMS
    public static final int USERNAME_MIN_LENGTH = 2;
    public static final int USERNAME_MAX_LENGTH = 50;
    public static final int FIRST_NAME_MIN_LENGTH = 2;
    public static final int FIRST_NAME_MAX_LENGTH = 50;
    public static final int LAST_NAME_MIN_LENGTH = 2;
    public static final int LAST_NAME_MAX_LENGTH = 50;
    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 15;
    public static final int STATUS_TYPE_MIN_LENGTH = 2;
    public static final int STATUS_TYPE_MAX_LENGTH = 15;
    public static final int JOB_MIN_LENGTH = 1;
    public static final int JOB_MAX_LENGTH = 50;

    // POST VALIDATION MESSAGES
    public static final String POST_TITLE_LENGTH_ERROR_MESSAGE = "Title must be between {min} and {max} characters long.";
    public static final String POST_CONTENT_LENGTH_ERROR_MESSAGE = "Content must be between {min} and {max} characters long.";
    public static final String POST_LOCATION_LENGTH_ERROR_MESSAGE = "Location must be up to {max} characters long.";
    public static final String UPDATE_POST_ERROR_MESSAGE_FORMAT = "User %s cannot modify somebody else's post.";
    public static final String DELETE_POST_ERROR_MESSAGE_FORMAT = "User %s cannot remove somebody else's post.";

    // POST VALIDATION PARAMS
    public static final int POST_TITLE_MIN_LENGTH = 2;
    public static final int POST_TITLE_MAX_LENGTH = 128;
    public static final int POST_CONTENT_MIN_LENGTH = 2;
    public static final int POST_CONTENT_MAX_LENGTH = 1024;
    public static final int POST_LOCATION_MAX_LENGTH = 60;

    // COMMENT VALIDATION MESSAGES
    public static final String COMMENT_TEXT_LENGTH_ERROR_MESSAGE = "Content must be between {min} and {max} characters long.";
    public static final String UPDATE_COMMENT_ERROR_MESSAGE_FORMAT = "User %s cannot modify somebody else's comment.";
    public static final String DELETE_COMMENT_ERROR_MESSAGE_FORMAT = "User %s cannot remove somebody else's comment.";

    // COMMENT VALIDATION PARAMS
    public static final int COMMENT_TEXT_MIN_LENGTH = 1;
    public static final int COMMENT_TEXT_MAX_LENGTH = 1024;

    // LIKE VALIDATION MESSAGES
    public static final String LIKE_DUPLICATE_ERROR_MESSAGE_FORMAT = "User %s has already liked the post '%s'";
    public static final String LIKE_EXISTS_ERROR_MESSAGE_FORMAT = "User %s has not liked post '%s', so the post cannot be unliked.";

    // GENDER VALIDATION MESSAGES
    public static final String GENDER_TYPE_ERROR_MESSAGE = "Gender type must be between {min} and {max} characters long.";

    // GENDER VALIDATION PARAMS
    public static final int GENDER_TYPE_MIN_LENGTH = 2;
    public static final int GENDER_TYPE_MAX_LENGTH = 20;

    // PROFILE VISIBILITY VALIDATION MESSAGES
    public static final String PROFILE_PRIVACY_TYPE_ERROR_MESSAGE = "Profile privacy type must be between {min} and {max} characters long.";

    // PROFILE VISIBILITY VALIDATION PARAMS
    public static final int PROFILE_PRIVACY_TYPE_MIN_LENGTH = 2;
    public static final int PROFILE_PRIVACY_TYPE_MAX_LENGTH = 20;
}
