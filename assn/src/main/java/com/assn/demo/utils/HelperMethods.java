package com.assn.demo.utils;

import org.springframework.security.core.Authentication;

public class HelperMethods {

    public static boolean hasAdminRole(Authentication authentication) {
        return authentication
                .getAuthorities()
                .stream()
                .anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
    }
}
