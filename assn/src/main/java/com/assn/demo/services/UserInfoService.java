package com.assn.demo.services;

import com.assn.demo.models.*;

import java.util.List;
import java.util.UUID;

public interface UserInfoService {

    UserInfo getById(UUID id);

    List<UserInfo> getFriendsOfUser(UUID userId);

    List<UserInfo> getFriendRequestsOfUser(UUID userId);

    List<UserInfo> getBlockList(UUID userId);

    UserInfo getByUsername(String username);

    UserInfo getByEmail(String email);

    List<UserInfo> getAll();

    List<UserInfo> getAllUsersByFullName(String firstName, String lastName);

    UserVisibilityDTO getUserInfoByVisibility(UUID userId, UserInfo user);

    List<UserInfo> getAllPublicVisibleUsers();

    UserInfo create(UserRegistrationDTORest user);

    void setUserEnabled(String username, boolean enabled);

    UserInfo emailConfirmationByRegistration(String token);

    Relationship requestFriendship(UUID senderId, UUID receiverId);

    Relationship acceptFriendship(UUID senderId, UserInfo receiver);

    Relationship blockFriend(UUID userToBlockId, UserInfo sender);

    UserInfo updateBasicUserInfo(UUID id, UserInfoDTO dto,
                                 UserInfo userToBeAuthenticated);

    UserInfo updateUserInfoDetails(UUID id, UserInfoDetailsDTO dto,
                                   UserInfo userToBeAuthenticated);

    UserInfo updatePicture(UUID userId, String picture);

    void changePassword(UserInfo user, PasswordChangeDTO passwordDTO,
                        UserInfo userToBeAuthenticated);

    void delete(UUID id, UserInfo user);

    void unfriendUser(UUID senderId, UserInfo receiver);

    void declineFriendship(UUID senderId, UserInfo receiver);

    void unblockFriend(UUID userToUnblockId, UserInfo sender);

    void createVerificationToken(UserInfo user, String token);
}
