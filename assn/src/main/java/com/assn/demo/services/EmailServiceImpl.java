package com.assn.demo.services;

import com.assn.demo.configurations.MailTrapConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    public static final String EMAIL_SENDER = "assn.info.contact@gmail.com";

    private MailTrapConfig mailTrapConfig;

    @Autowired
    public EmailServiceImpl(MailTrapConfig mailTrapConfig) {
        this.mailTrapConfig = mailTrapConfig;
    }

    @Override
    public void sendEmailViaMailTrap(String recipientEmail, String title, String text, String URL) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailTrapConfig.getHost());
        mailSender.setPort(mailTrapConfig.getPort());
        mailSender.setUsername(mailTrapConfig.getUsername());
        mailSender.setPassword(mailTrapConfig.getPassword());

        SimpleMailMessage mailToSend = getMailMessage(recipientEmail, text, title, URL);

        mailSender.send(mailToSend);
    }

    private SimpleMailMessage getMailMessage(String recipientEmail, String title, String message, String URL) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setFrom(EMAIL_SENDER);
        mailMessage.setTo(recipientEmail);
        mailMessage.setSubject(title);
        mailMessage.setText(message + "http://localhost:8080" + URL);

        return mailMessage;
    }
}
