package com.assn.demo.services;

import com.assn.demo.models.Gender;
import java.util.List;
import java.util.UUID;

public interface GenderService {

    Gender getById(UUID id);

    Gender getByType(String type);

    List<Gender> getAll();
}
