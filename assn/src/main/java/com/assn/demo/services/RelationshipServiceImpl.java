package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Relationship;
import com.assn.demo.repos.RelationshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RelationshipServiceImpl implements RelationshipService {

    private RelationshipRepository relationshipRepository;

    @Autowired
    public RelationshipServiceImpl(RelationshipRepository relationshipRepository) {
        this.relationshipRepository = relationshipRepository;
    }

    @Override
    public Relationship getById(UUID id) {
        return relationshipRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Relationship", "id", String.valueOf(id)));
    }

    @Override
    public List<Relationship> getAll() {
        return relationshipRepository.findAll();
    }

    @Override
    public int getRelationshipStatusId(UUID receiver, UUID sender) {
        return Optional.ofNullable(relationshipRepository.getRelationshipByReceiverIdAndSenderId(receiver, sender))
                .map(o -> o.getStatusType().getStatusId())
                .orElse(-1);
    }
}
