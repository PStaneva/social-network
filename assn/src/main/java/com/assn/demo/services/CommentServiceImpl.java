package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.InconformityException;
import com.assn.demo.models.*;
import com.assn.demo.repos.CommentRepository;
import com.assn.demo.repos.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.UUID;

import static com.assn.demo.utils.HelperMethods.hasAdminRole;
import static com.assn.demo.utils.ValidationMessages.DELETE_COMMENT_ERROR_MESSAGE_FORMAT;
import static com.assn.demo.utils.ValidationMessages.UPDATE_COMMENT_ERROR_MESSAGE_FORMAT;

@Service
public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository;
    private PostRepository postRepository;
    private Mapper mapper;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository,
                              PostRepository postRepository,
                              Mapper mapper) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
        this.mapper = mapper;
    }

    @Override
    public Comment getById(UUID id) {
        return commentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Comment", "id", String.valueOf(id)));
    }

    @Override
    public List<Comment> getAllCommentsByPost(UUID postId) {
        List<Comment> result;
        if (postRepository.existsById(postId)) {
            result = commentRepository.findAllByPostOrderByCreatedAtDesc(postId);
        } else {
            throw new EntityNotFoundException("Post", "id", String.valueOf(postId));
        }

        return result;
    }

    @Override
    public List<Comment> getAllSortedChronologically() {
        return commentRepository.findAllByOrderByCreatedAtAsc();
    }

    @Override
    public Comment create(UUID postId, CommentDTO commentDTO, UserInfo createdBy) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new EntityNotFoundException("Post", "id", String.valueOf(postId)));
        Comment comment = mapper.toComment(commentDTO, post, createdBy);

        return commentRepository.save(comment);
    }

    @Override
    public Comment addResponseToComment(UUID commentId, CommentDTO commentDTO, UserInfo createdBy) {
        Comment parentComment = getById(commentId);
        Comment responseToComment = mapper.toComment(commentDTO, parentComment.getPost(), createdBy);
        responseToComment.setParentComment(parentComment);

        return commentRepository.save(responseToComment);
    }

    @Override
    public Comment update(UUID commentId, CommentDTO newCommentDTO, UserInfo user) {
        Comment oldComment = getById(commentId);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!oldComment.getCreatedBy().equals(user) && !hasAdminRole(authentication)) {
            throw new InconformityException(String
                    .format(UPDATE_COMMENT_ERROR_MESSAGE_FORMAT, user.getUsername()));
        }

        Comment newComment = mapper.mergeComments(oldComment, newCommentDTO);
        return commentRepository.save(newComment);
    }

    @Override
    @Transactional
    public void delete(UUID commentId, UserInfo user) {
        Comment comment = getById(commentId);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!comment.getCreatedBy().equals(user) && !hasAdminRole(authentication)) {
            throw new InconformityException(String
                    .format(DELETE_COMMENT_ERROR_MESSAGE_FORMAT, user.getUsername()));
        }

        List<Comment> allComments = commentRepository.findAllByParentComment(commentId);
        if (!allComments.isEmpty()) {
            allComments.get(0).setParentComment(comment.getParentComment());
        }

        commentRepository.delete(comment);
    }
}
