package com.assn.demo.services;

import com.assn.demo.exeptions.DuplicateEntityException;
import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Like;
import com.assn.demo.models.Mapper;
import com.assn.demo.models.Post;
import com.assn.demo.models.UserInfo;
import com.assn.demo.repos.LikeRepository;
import com.assn.demo.repos.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import static com.assn.demo.utils.ValidationMessages.LIKE_DUPLICATE_ERROR_MESSAGE_FORMAT;
import static com.assn.demo.utils.ValidationMessages.LIKE_EXISTS_ERROR_MESSAGE_FORMAT;

@Service
public class LikeServiceImpl implements LikeService {

    private LikeRepository likeRepository;
    private PostRepository postRepository;
    private Mapper mapper;

    @Autowired
    public LikeServiceImpl(LikeRepository likeRepository,
                           PostRepository postRepository,
                           Mapper mapper) {
        this.likeRepository = likeRepository;
        this.postRepository = postRepository;
        this.mapper = mapper;
    }

    public List<UserInfo> getAllUsersLikedPost(UUID postId) {
        postRepository.findById(postId)
                .orElseThrow(() -> new EntityNotFoundException("Post", "id", String.valueOf(postId)));
        List<Like> likes = likeRepository.findLikesByPost(postId);

        return likes
                .stream()
                .map(Like::getUserInfo)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void likePost(UUID postId, UserInfo user) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new EntityNotFoundException("Post", "id", String.valueOf(postId)));

        boolean isPresent = likeRepository.findLikeByPostAndUser(postId, user.getId()).isPresent();

        if (isPresent) {
            throw new DuplicateEntityException(String
                    .format(LIKE_DUPLICATE_ERROR_MESSAGE_FORMAT, user.getUsername(), post.getTitle()));
        }

        Like like = mapper.toLike(post, user);
        likeRepository.save(like);
        int likesCount = likeRepository.findLikesByPost(postId).size();
        postRepository.setLikesCount(postId, likesCount);
    }

    @Override
    @Transactional
    public void unlikePost(UUID postId, UserInfo user) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new EntityNotFoundException("Post", "id", String.valueOf(postId)));

        Like like = likeRepository.findLikeByPostAndUser(postId, user.getId())
                .orElseThrow(() -> new EntityNotFoundException(String
                        .format(LIKE_EXISTS_ERROR_MESSAGE_FORMAT, user.getUsername(), post.getTitle())));

        likeRepository.delete(like);
        int likesCount = likeRepository.findLikesByPost(postId).size();
        postRepository.setLikesCount(postId, likesCount);
    }

    @Override
    public boolean isPostLikedByUser(UUID postId, UserInfo user) {
        return getAllUsersLikedPost(postId)
                .stream()
                .anyMatch(o -> o.equals(user));
    }
}
