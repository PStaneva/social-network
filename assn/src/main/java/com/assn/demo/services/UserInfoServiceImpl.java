package com.assn.demo.services;

import com.assn.demo.exeptions.DuplicateEntityException;
import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.PasswordMismatchException;
import com.assn.demo.exeptions.RelationshipConflictException;
import com.assn.demo.models.*;
import com.assn.demo.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.assn.demo.enums.StatusTypeEnum.*;
import static com.assn.demo.utils.HelperMethods.hasAdminRole;
import static com.assn.demo.utils.ValidationMessages.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    private UserInfoRepository userInfoRepository;
    private StatusTypeRepository statusTypeRepository;
    private RelationshipRepository relationshipRepository;
    private PostRepository postRepository;
    private LikeRepository likeRepository;
    private CommentRepository commentRepository;
    private ProfileVisibilityRepository visibilityRepository;
    private VerificationTokenRepository tokenRepository;
    private UserDetailsManager userDetailsManager;
    private BCryptPasswordEncoder passwordEncoder;
    private Mapper mapper;

    @Autowired
    public UserInfoServiceImpl(UserInfoRepository userInfoRepository,
                               StatusTypeRepository statusTypeRepository,
                               RelationshipRepository relationshipRepository,
                               PostRepository postRepository, LikeRepository likeRepository,
                               CommentRepository commentRepository,
                               ProfileVisibilityRepository visibilityRepository,
                               VerificationTokenRepository tokenRepository,
                               UserDetailsManager userDetailsManager,
                               BCryptPasswordEncoder passwordEncoder, Mapper mapper) {
        this.userInfoRepository = userInfoRepository;
        this.statusTypeRepository = statusTypeRepository;
        this.relationshipRepository = relationshipRepository;
        this.postRepository = postRepository;
        this.likeRepository = likeRepository;
        this.commentRepository = commentRepository;
        this.visibilityRepository = visibilityRepository;
        this.tokenRepository = tokenRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
    }

    @Override
    public UserInfo getById(UUID id) {
        return userInfoRepository.getById(id);
    }

    @Override
    public List<UserInfo> getFriendsOfUser(UUID userId) {
        UserInfo receiver = userInfoRepository.getById(userId);
        return userInfoRepository.getFriendsOfUser(receiver);
    }

    @Override
    public List<UserInfo> getFriendRequestsOfUser(UUID userId) {
        UserInfo user = userInfoRepository.getById(userId);
        return userInfoRepository.getFriendRequestsOfUser(user);
    }

    @Override
    public List<UserInfo> getBlockList(UUID userId) {
        UserInfo user = userInfoRepository.getById(userId);
        return userInfoRepository.getBlockList(user);
    }

    @Override
    public UserInfo getByUsername(String username) {
        if (!userInfoRepository.checkUserExistsByUsername(username)) {
            throw new EntityNotFoundException("User", "username", username);
        }

        return userInfoRepository.getByUsername(username).get(0);
    }

    @Override
    public UserInfo getByEmail(String email) {
        if (!userInfoRepository.checkUserExistsByEmail(email)) {
            throw new EntityNotFoundException("User", "email", email);
        }

        return userInfoRepository.getByEmail(email).get(0);
    }

    @Override
    public List<UserInfo> getAll() {
        return userInfoRepository.getAll();
    }

    @Override
    public List<UserInfo> getAllUsersByFullName(String firstName, String lastName) {
        return userInfoRepository.getAllUsersByFullName(firstName, lastName);
    }

    @Override
    public UserVisibilityDTO getUserInfoByVisibility(UUID userId, UserInfo sender) {
        UserInfo user = userInfoRepository.getById(userId);
        UserVisibilityDTO userToReturn;
        StatusType status = statusTypeRepository.getByType(FRIENDS.name());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if ((user.getProfileVisibility().getType().equalsIgnoreCase("EVERYONE"))
                || ((user.getProfileVisibility().getType().equalsIgnoreCase("CONNECTED PROFILES") &&
                userInfoRepository.checkRelationshipExists(sender, user, status))
                || hasAdminRole(authentication))) {
            userToReturn = mapper.toUserVisibilityDTOWithAdditionalFields(user);
        } else {
            userToReturn = mapper.toUserVisibilityDTOWithRequiredFields(user);
        }

        return userToReturn;
    }

    @Override
    public List<UserInfo> getAllPublicVisibleUsers() {
        return userInfoRepository.getAllPublicVisibleUsers();
    }

    @Override
    public UserInfo create(UserRegistrationDTORest user) {
        if (userInfoRepository.checkUserExistsByUsername(user.getUsername())) {
            throw new DuplicateEntityException("User", user.getUsername());
        }

        if (userInfoRepository.checkUserExistsByEmail(user.getEmail())) {
            throw new DuplicateEntityException("User's email", user.getEmail());
        }

        UserInfo userToCreate = mapper.toUserInfo(user);

        ProfileVisibility profileVisibility = visibilityRepository.findByType("ONLY ME")
                .orElseThrow(() -> new EntityNotFoundException("Visibility", "type", "ONLY ME"));
        userToCreate.setProfileVisibility(profileVisibility);

        userInfoRepository.create(userToCreate);

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newDetailsUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(), passwordEncoder.encode(user.getPassword()),
                        false, true, true, true, authorities);

        userDetailsManager.createUser(newDetailsUser);

        return userToCreate;
    }

    @Override
    public void setUserEnabled(String username, boolean enabled) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

        org.springframework.security.core.userdetails.User userToBeDisabled =
                new org.springframework.security.core.userdetails.User(
                        username, userDetailsManager.loadUserByUsername(username).getPassword(),
                        enabled, true, true, true, authorities);

        userDetailsManager.updateUser(userToBeDisabled);
    }

    @Override
    public UserInfo emailConfirmationByRegistration(String token) {
        VerificationToken verificationToken = tokenRepository.findByToken(token)
                .orElseThrow(() -> new EntityNotFoundException("Token", "id", token));
        UserInfo user = verificationToken.getUser();
        Calendar calendar = Calendar.getInstance();

        if ((verificationToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) {
            delete(user.getId(), user);
            throw new EntityNotFoundException(EXPIRED_AUTHENTICATION_ERROR_MESSAGE);
        }

        setUserEnabled(user.getUsername(), true);
        tokenRepository.delete(verificationToken);
        return user;
    }

    @Override
    public Relationship requestFriendship(UUID senderId, UUID receiverId) {
        UserInfo sender = userInfoRepository.getById(senderId);
        UserInfo receiver = userInfoRepository.getById(receiverId);
        StatusType pendingType = statusTypeRepository.getByType(PENDING.name());
        StatusType blockedType = statusTypeRepository.getByType(BLOCKED.name());

        if (sender.getUsername().equals(receiver.getUsername())) {
            throw new DuplicateEntityException(RELATIONSHIP_ERROR_MESSAGE);
        }

        if (userInfoRepository.checkRelationshipExists(sender, receiver, pendingType) |
                userInfoRepository.checkRelationshipExists(receiver, sender, blockedType)) {
            throw new DuplicateEntityException(String.format(RELATIONSHIP_DUPLICATE_MESSAGE_FORMAT,
                    sender.getUsername(), receiver.getUsername()));
        }

        Relationship relationship = mapper.toRelationship(sender, receiver, pendingType);
        return relationshipRepository.save(relationship);
    }

    @Override
    public Relationship acceptFriendship(UUID senderId, UserInfo receiver) {
        UserInfo sender = userInfoRepository.getById(senderId);
        StatusType pendingType = statusTypeRepository.getByType(PENDING.name());

        if (sender.getUsername().equals(receiver.getUsername())) {
            throw new RelationshipConflictException(CONFLICT_BY_ACCEPT_ERROR_MESSAGE);
        }

        List<Relationship> relationships = relationshipRepository.findAll();
        relationships = relationships.stream()
                .filter(r -> r.getSender().equals(sender) && r.getReceiver().equals(receiver) && r.getStatusType().equals(pendingType))
                .collect(Collectors.toList());

        if (relationships.size() == 0) {
            throw new EntityNotFoundException(String
                    .format(RELATIONSHIP_EXIST_ERROR_MESSAGE_FORMAT, sender.getUsername()));
        }

        Relationship relationship = relationships.get(0);

        StatusType statusType = statusTypeRepository.getByType(FRIENDS.name());
        relationship.setStatusType(statusType);
        return userInfoRepository.updateRelationship(relationship);
    }

    @Override
    public Relationship blockFriend(UUID userToBlockId, UserInfo sender) {
        UserInfo userToBeBlocked = userInfoRepository.getById(userToBlockId);

        if (userToBeBlocked.equals(sender)) {
            throw new RelationshipConflictException(BLOCK_YOURSELF_ERROR_MESSAGE);
        }

        StatusType friendsType = statusTypeRepository.getByType(FRIENDS.name());
        List<Relationship> relationships = relationshipRepository.findAll();
        relationships = relationships.stream()
                .filter(r -> (r.getSender().equals(sender) && r.getReceiver().equals(userToBeBlocked) && r.getStatusType().equals(friendsType))
                        || (r.getSender().equals(userToBeBlocked) && r.getReceiver().equals(sender) && r.getStatusType().equals(friendsType)))
                .collect(Collectors.toList());

        if (relationships.size() == 0) {
            throw new EntityNotFoundException(String
                    .format(BLOCK_NONFRIEND_ERROR_MESSAGE_FORMAT, userToBeBlocked.getUsername()));
        }

        StatusType statusType = statusTypeRepository.getByType(BLOCKED.name());
        Relationship relationship = relationships.get(0);
        relationship.setStatusType(statusType);
        relationship.setSender(sender);
        relationship.setReceiver(userToBeBlocked);
        return userInfoRepository.updateRelationship(relationship);
    }

    @Override
    public UserInfo updateBasicUserInfo(UUID id, UserInfoDTO dto,
                                        UserInfo userToBeAuthenticated) {
        UserInfo oldUser = userInfoRepository.getById(id);
        UserInfo newUser = mapper.mergeUserInfos(oldUser, dto);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!userToBeAuthenticated.getUsername().equals(oldUser.getUsername()) && !hasAdminRole(authentication)) {
            throw new ResponseStatusException(BAD_REQUEST, String
                    .format(USER_UPDATE_PERMISSION_MESSAGE_FORMAT, userToBeAuthenticated.getUsername()));
        }

        if (userInfoRepository.checkUserExistsByUsername(newUser.getUsername()) && !oldUser.getUsername().equals(newUser.getUsername())) {
            throw new DuplicateEntityException("Username", newUser.getUsername());
        }

        if (userInfoRepository.checkUserExistsByEmail(newUser.getEmail()) && !oldUser.getEmail().equals(newUser.getEmail())) {
            throw new DuplicateEntityException("User's email", newUser.getEmail());
        }

        UserDetails user = userDetailsManager.loadUserByUsername(oldUser.getUsername());
        userDetailsManager.deleteUser(oldUser.getUsername());
        userInfoRepository.update(newUser);

        org.springframework.security.core.userdetails.User newDetailsUser =
                new org.springframework.security.core.userdetails.User(
                        newUser.getUsername(), user.getPassword(), user.getAuthorities());

        userDetailsManager.createUser(newDetailsUser);

        return newUser;
    }

    @Override
    public UserInfo updateUserInfoDetails(UUID id, UserInfoDetailsDTO dto,
                                          UserInfo userToBeAuthenticated) {
        UserInfo oldUser = userInfoRepository.getById(id);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!userToBeAuthenticated.getUsername().equals(oldUser.getUsername()) && !hasAdminRole(authentication)) {
            throw new ResponseStatusException(BAD_REQUEST, String
                    .format(USER_UPDATE_PERMISSION_MESSAGE_FORMAT, userToBeAuthenticated.getUsername()));
        }

        UserInfo newUser = mapper.mergeUserInfoDetailsDTO(oldUser, dto);
        return userInfoRepository.update(newUser);
    }

    @Override
    public UserInfo updatePicture(UUID userId, String picture) {
        UserInfo user = userInfoRepository.getById(userId);
        user.setPicture(picture);
        return userInfoRepository.update(user);
    }

    @Override
    public void changePassword(UserInfo user, PasswordChangeDTO passwordDTO,
                               UserInfo userToBeAuthenticated) {
        if (!userToBeAuthenticated.getUsername().equals(user.getUsername())) {
            throw new ResponseStatusException(BAD_REQUEST, String
                    .format(USER_UPDATE_PERMISSION_MESSAGE_FORMAT, userToBeAuthenticated.getUsername()));
        }

        UserDetails userDetails = userDetailsManager.loadUserByUsername(user.getUsername());

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(passwordDTO.getCurrentPassword(), userDetails.getPassword())) {
            throw new PasswordMismatchException(CURRENT_PASSWORD_MISMATCH_MESSAGE);
        }

        if (!passwordDTO.getNewPassword().equals(passwordDTO.getConfirmPassword())) {
            throw new PasswordMismatchException(PASSWORD_MISMATCH_MESSAGE);
        }

        userInfoRepository.changePassword(user, passwordDTO);
    }

    @Override
    public void delete(UUID id, UserInfo user) {
        UserInfo userToBeDeleted = getById(id);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!user.getUsername().equals(userToBeDeleted.getUsername()) && !hasAdminRole(authentication)) {
            throw new ResponseStatusException(BAD_REQUEST, String
                    .format(USER_DELETE_PERMISSION_MESSAGE_FORMAT, user.getUsername()));
        }

        likeRepository.deleteByUserInfo_Id(id);
        commentRepository.deleteByCreatedById(id);
        postRepository.deleteByCreatedById(id);
        relationshipRepository.deleteByUser(id);
        tokenRepository.deleteByUser(id);
        userDetailsManager.deleteUser(userToBeDeleted.getUsername());
        userInfoRepository.delete(id);
    }

    @Override
    public void unfriendUser(UUID senderId, UserInfo receiver) {
        UserInfo sender = userInfoRepository.getById(senderId);
        StatusType statusType = statusTypeRepository.getByType(FRIENDS.name());

        if (receiver.getUsername().equals(sender.getUsername())) {
            throw new RelationshipConflictException(CONFLICT_BY_REMOVE_FRIEND_ERROR_MESSAGE);
        }

        List<Relationship> relationships = relationshipRepository.findAll();
        relationships = relationships.stream()
                .filter((r -> ((r.getSender().equals(sender) && r.getReceiver().equals(receiver))
                        || (r.getSender().equals(receiver) && r.getReceiver().equals(sender)))
                        && r.getStatusType().equals(statusType)))
                .collect(Collectors.toList());

        if (relationships.size() == 0) {
            throw new EntityNotFoundException(String
                    .format(FRIEND_NOT_EXIST_ERROR_MESSAGE_FORMAT, sender.getUsername()));
        }

        relationshipRepository.delete(relationships.get(0));
    }

    @Override
    public void declineFriendship(UUID senderId, UserInfo receiver) {
        UserInfo sender = userInfoRepository.getById(senderId);
        StatusType statusType = statusTypeRepository.getByType(PENDING.name());

        if (receiver.getUsername().equals(sender.getUsername())) {
            throw new RelationshipConflictException(CONFLICT_BY_DECLINE_ERROR_MESSAGE);
        }

        List<Relationship> relationships = relationshipRepository.findAll();
        relationships = relationships.stream()
                .filter(r -> r.getSender().equals(sender) && r.getReceiver().equals(receiver) && r.getStatusType().equals(statusType))
                .collect(Collectors.toList());

        if (relationships.size() == 0) {
            throw new EntityNotFoundException(String
                    .format(RELATIONSHIP_EXIST_ERROR_MESSAGE_FORMAT, sender.getUsername()));
        }

        relationshipRepository.delete(relationships.get(0));
    }

    @Override
    public void unblockFriend(UUID userToUnblockId, UserInfo sender) {
        UserInfo userToBeUnBlocked = userInfoRepository.getById(userToUnblockId);

        if (userToBeUnBlocked.equals(sender)) {
            throw new RelationshipConflictException(UNBLOCK_YOURSELF_ERROR_MESSAGE);
        }

        StatusType blockedType = statusTypeRepository.getByType(BLOCKED.name());
        List<Relationship> relationships = relationshipRepository.findAll();
        relationships = relationships.stream()
                .filter(r -> (r.getSender().equals(sender) && r.getReceiver().equals(userToBeUnBlocked)
                        && r.getStatusType().equals(blockedType)))
                .collect(Collectors.toList());

        if (relationships.size() == 0) {
            throw new EntityNotFoundException(String
                    .format(UNBLOCK_ERROR_MESSAGE_FORMAT, userToBeUnBlocked.getUsername(), userToBeUnBlocked.getUsername()));
        }

        relationshipRepository.delete(relationships.get(0));
    }

    @Override
    public void createVerificationToken(UserInfo user, String token) {
        VerificationToken myToken = new VerificationToken(user, token);
        tokenRepository.save(myToken);
    }
}