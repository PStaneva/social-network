package com.assn.demo.services;

import com.assn.demo.models.Comment;
import com.assn.demo.models.CommentDTO;
import com.assn.demo.models.UserInfo;
import java.util.List;
import java.util.UUID;

public interface CommentService {

    Comment getById(UUID id);

    List<Comment> getAllCommentsByPost(UUID postId);

    List<Comment> getAllSortedChronologically();

    Comment create(UUID postId, CommentDTO commentDTO, UserInfo createdBy);

    Comment addResponseToComment(UUID commentId, CommentDTO commentDTO, UserInfo createdBy);

    Comment update(UUID commentId, CommentDTO newCommentDTO, UserInfo user);

    void delete(UUID commentId, UserInfo user);
}
