package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.ProfileVisibility;
import com.assn.demo.repos.ProfileVisibilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

@Service
public class ProfileVisibilityServiceImpl implements ProfileVisibilityService {

    private ProfileVisibilityRepository profileVisibilityRepository;

    @Autowired
    public ProfileVisibilityServiceImpl(ProfileVisibilityRepository profileVisibilityRepository) {
        this.profileVisibilityRepository = profileVisibilityRepository;
    }

    @Override
    public ProfileVisibility getById(UUID id) {
        return profileVisibilityRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Visibility", "id", String.valueOf(id)));
    }

    @Override
    public ProfileVisibility getByType(String type) {
        return profileVisibilityRepository.findByType(type)
                .orElseThrow(() -> new EntityNotFoundException("Visibility", "type", type));
    }

    @Override
    public List<ProfileVisibility> getAll() {
        return profileVisibilityRepository.findAll();
    }
}
