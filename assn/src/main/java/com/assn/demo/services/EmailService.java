package com.assn.demo.services;

public interface EmailService {

    void sendEmailViaMailTrap(String recipientEmail, String title, String message, String URL);
}
