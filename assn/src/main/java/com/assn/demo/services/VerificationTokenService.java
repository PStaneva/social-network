package com.assn.demo.services;

import com.assn.demo.models.VerificationToken;

public interface VerificationTokenService {

    VerificationToken getVerificationToken(String token);
}
