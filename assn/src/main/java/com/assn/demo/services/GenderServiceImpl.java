package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Gender;
import com.assn.demo.repos.GenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

@Service
public class GenderServiceImpl implements GenderService {

    private GenderRepository genderRepository;

    @Autowired
    public GenderServiceImpl(GenderRepository genderRepository) {
        this.genderRepository = genderRepository;
    }

    @Override
    public Gender getById(UUID id) {
        return genderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Gender", "id", String.valueOf(id)));
    }

    @Override
    public Gender getByType(String type) {
        return genderRepository.findByType(type)
                .orElseThrow(() -> new EntityNotFoundException("Gender", "type", type));
    }

    @Override
    public List<Gender> getAll() {
        return genderRepository.findAll();
    }
}
