package com.assn.demo.services;

import com.assn.demo.models.Post;
import com.assn.demo.models.PostDTO;
import com.assn.demo.models.UserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

import java.util.List;
import java.util.UUID;

public interface PostService {

    Post getById(UUID id);

    List<Post> getAllPostsByUser(UUID id);

    List<Post> getAllPublicVisibleUsersPosts();

    List<Post> getAll();

    List<Post> getAllPostFeed(UUID userId);

    Page<Post> getAllPostFeedPaginated(UUID userId, @PageableDefault(value = 10) Pageable pageable);

    Post create(PostDTO postDTO, UserInfo createdBy);

    Post update(UUID postId, PostDTO newPostDTO, UserInfo user);

    void delete(UUID postId, UserInfo user);

    List<Post> showPostsSortedByDateAsc();

    List<Post> showPostsSortedByDateDesc();
}

