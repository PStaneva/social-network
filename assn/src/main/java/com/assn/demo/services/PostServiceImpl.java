package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.InconformityException;
import com.assn.demo.models.Mapper;
import com.assn.demo.models.Post;
import com.assn.demo.models.PostDTO;
import com.assn.demo.models.UserInfo;
import com.assn.demo.repos.CommentRepository;
import com.assn.demo.repos.LikeRepository;
import com.assn.demo.repos.PostRepository;
import com.assn.demo.repos.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.assn.demo.utils.HelperMethods.hasAdminRole;
import static com.assn.demo.utils.ValidationMessages.DELETE_POST_ERROR_MESSAGE_FORMAT;
import static com.assn.demo.utils.ValidationMessages.UPDATE_POST_ERROR_MESSAGE_FORMAT;
import static java.time.temporal.ChronoUnit.HOURS;

@Service
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;
    private UserInfoRepository userInfoRepository;
    private CommentRepository commentRepository;
    private LikeRepository likeRepository;
    private Mapper mapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository,
                           UserInfoRepository userInfoRepository,
                           CommentRepository commentRepository,
                           LikeRepository likeRepository,
                           Mapper mapper) {
        this.postRepository = postRepository;
        this.userInfoRepository = userInfoRepository;
        this.commentRepository = commentRepository;
        this.likeRepository = likeRepository;
        this.mapper = mapper;
    }

    @Override
    public Post getById(UUID id) {
        return postRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Post", "id", String.valueOf(id)));
    }

    @Override
    public List<Post> getAllPostsByUser(UUID id) {
        userInfoRepository.checkUserExistsById(id);
        return postRepository.findAllByCreatedBy(id);
    }

    @Override
    public List<Post> getAllPublicVisibleUsersPosts() {
        return postRepository.findAllPublicVisibleUsersPosts();
    }

    @Override
    public List<Post> getAll() {
        return postRepository.findAll();
    }

    @Override
    public List<Post> getAllPostFeed(UUID userId) {
        UserInfo user = userInfoRepository.getById(userId);
        return feedAlgorithm(userInfoRepository.getAllPostFeed(user));
    }

    @Override
    public Page<Post> getAllPostFeedPaginated(UUID userId,
                                              @PageableDefault(value = 10) Pageable pageable) {
        UserInfo user = userInfoRepository.getById(userId);
        List<Post> posts = feedAlgorithm(userInfoRepository.getAllPostFeed(user));

        int start = (int) pageable.getOffset();
        int end = (start + pageable.getPageSize()) > posts.size() ? posts.size() : (int) (start + pageable.getPageSize());
        return new PageImpl<>(posts.subList(start, end), pageable, posts.size());
    }

    @Override
    public Post create(PostDTO postDTO, UserInfo createdBy) {
        Post post = mapper.toPost(postDTO, createdBy);
        return postRepository.save(post);
    }

    @Override
    public Post update(UUID postId, PostDTO newPostDTO, UserInfo user) {
        Post oldPost = getById(postId);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!oldPost.getCreatedBy().equals(user) && !hasAdminRole(authentication)) {
            throw new InconformityException(String
                    .format(UPDATE_POST_ERROR_MESSAGE_FORMAT, user.getUsername()));
        }

        Post newPost = mapper.mergePosts(oldPost, newPostDTO);
        return postRepository.save(newPost);
    }

    @Transactional
    @Override
    public void delete(UUID postId, UserInfo user) {
        Post post = getById(postId);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!post.getCreatedBy().equals(user) && !hasAdminRole(authentication)) {
            throw new InconformityException(String
                    .format(DELETE_POST_ERROR_MESSAGE_FORMAT, user.getUsername()));
        }

        commentRepository.deleteByPostId(postId);
        likeRepository.deleteByPostId(postId);
        postRepository.delete(post);
    }

    @Override
    public List<Post> showPostsSortedByDateAsc() {
        return postRepository.findAllByOrderByCreatedAtAsc();
    }

    @Override
    public List<Post> showPostsSortedByDateDesc() {
        return postRepository.findAllByOrderByCreatedAtDesc();
    }

    private List<Post> feedAlgorithm(List<Post> postList) {
        return postList
                .stream()
                .sorted((p1, p2) -> {
                    LocalDateTime creationDate_1 = p1.getCreatedAt();
                    LocalDateTime creationDate_2 = p2.getCreatedAt();

                    long interactions_1 = p1.getLikesCount() + p1.getComments().size();
                    long interactions_2 = p2.getLikesCount() + p2.getComments().size();

                    long hoursDifference_1 = creationDate_1.until(LocalDateTime.now(), HOURS);
                    long hoursDifference_2 = creationDate_2.until(LocalDateTime.now(), HOURS);

                    long result_1 = interactions_1 - hoursDifference_1;
                    long result_2 = interactions_2 - hoursDifference_2;

                    return Long.compare(result_2, result_1);
                }).collect(Collectors.toList());
    }
}
