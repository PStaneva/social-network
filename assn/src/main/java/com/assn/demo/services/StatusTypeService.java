package com.assn.demo.services;

import com.assn.demo.models.StatusType;
import java.util.List;

public interface StatusTypeService {

    StatusType getById(int id);

    StatusType getByType(String statusType);

    List<StatusType> getAll();
}
