package com.assn.demo.services;

import com.assn.demo.models.Relationship;
import java.util.List;
import java.util.UUID;

public interface RelationshipService {

    Relationship getById(UUID id);

    List<Relationship> getAll();

    int getRelationshipStatusId(UUID receiver, UUID sender);
}
