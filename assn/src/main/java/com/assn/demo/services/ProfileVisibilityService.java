package com.assn.demo.services;

import com.assn.demo.models.ProfileVisibility;
import java.util.List;
import java.util.UUID;

public interface ProfileVisibilityService {

    ProfileVisibility getById(UUID id);

    ProfileVisibility getByType(String type);

    List<ProfileVisibility> getAll();
}
