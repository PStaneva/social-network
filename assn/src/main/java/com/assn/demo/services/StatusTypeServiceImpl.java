package com.assn.demo.services;

import com.assn.demo.models.StatusType;
import com.assn.demo.repos.StatusTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class StatusTypeServiceImpl implements StatusTypeService {

    private StatusTypeRepository statusTypeRepository;

    @Autowired
    public StatusTypeServiceImpl(StatusTypeRepository statusTypeRepository) {
        this.statusTypeRepository = statusTypeRepository;
    }

    @Override
    public StatusType getById(int id) {
        return statusTypeRepository.getById(id);
    }

    @Override
    public StatusType getByType(String statusType) {
        return statusTypeRepository.getByType(statusType);
    }

    @Override
    public List<StatusType> getAll() {
        return statusTypeRepository.getAll();
    }
}
