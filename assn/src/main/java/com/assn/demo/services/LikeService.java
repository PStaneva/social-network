package com.assn.demo.services;

import com.assn.demo.models.UserInfo;
import java.util.List;
import java.util.UUID;

public interface LikeService {

    List<UserInfo> getAllUsersLikedPost(UUID postId);

    void likePost(UUID postId, UserInfo user);

    void unlikePost(UUID postId, UserInfo user);

    boolean isPostLikedByUser(UUID postId, UserInfo user);
}
