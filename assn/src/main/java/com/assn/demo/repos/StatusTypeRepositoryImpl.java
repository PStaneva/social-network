package com.assn.demo.repos;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.StatusType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class StatusTypeRepositoryImpl implements StatusTypeRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StatusTypeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public StatusType getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            StatusType statusType = session.get(StatusType.class, id);
            if (statusType == null) {
                throw new EntityNotFoundException("Status", "id", String.valueOf(id));
            }
            return statusType;
        }
    }

    @Override
    public StatusType getByType(String statusType) {
        try (Session session = sessionFactory.openSession()) {
            Query<StatusType> query = session.createQuery(
                    "SELECT s FROM StatusType s WHERE s.statusType LIKE :statusType", StatusType.class);
            query.setParameter("statusType", statusType);

            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Status", "type", statusType);
            }

            return query.list().get(0);
        }
    }

    @Override
    public List<StatusType> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("SELECT s FROM StatusType s", StatusType.class)
                    .list();
        }
    }
}
