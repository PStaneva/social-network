package com.assn.demo.repos;

import com.assn.demo.models.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface GenderRepository extends JpaRepository<Gender, UUID> {

    Optional<Gender> findByType(String type);
}
