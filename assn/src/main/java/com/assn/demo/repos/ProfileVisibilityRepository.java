package com.assn.demo.repos;

import com.assn.demo.models.ProfileVisibility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProfileVisibilityRepository extends JpaRepository<ProfileVisibility, UUID> {

    Optional<ProfileVisibility> findByType(String type);
}
