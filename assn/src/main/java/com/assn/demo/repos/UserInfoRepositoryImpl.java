package com.assn.demo.repos;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.assn.demo.enums.StatusTypeEnum.*;

@Repository
public class UserInfoRepositoryImpl implements UserInfoRepository {

    private SessionFactory sessionFactory;
    private PostRepository postRepository;
    private UserDetailsManager userDetailsManager;
    private BCryptPasswordEncoder passwordEncoder;
    private StatusTypeRepository statusTypeRepository;

    @Autowired
    public UserInfoRepositoryImpl(SessionFactory sessionFactory,
                                  PostRepository postRepository,
                                  UserDetailsManager userDetailsManager,
                                  BCryptPasswordEncoder passwordEncoder,
                                  StatusTypeRepository statusTypeRepository) {
        this.sessionFactory = sessionFactory;
        this.postRepository = postRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.statusTypeRepository = statusTypeRepository;
    }

    @Override
    public UserInfo getById(UUID id) {
        try (Session session = sessionFactory.openSession()) {
            UserInfo user = session.get(UserInfo.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", "id", String.valueOf(id));
            }
            return user;
        }
    }

    @Override
    public List<UserInfo> getFriendsOfUser(UserInfo receiver) {
        StatusType statusType = statusTypeRepository.getByType(FRIENDS.name());
        try (Session session = sessionFactory.openSession()) {
            Query<Relationship> query = session.createQuery(
                    "select r from Relationship r where (r.receiver = :receiver AND r.statusType = :statusType) OR (r.sender = :receiver AND r.statusType = :statusType)",
                    Relationship.class);
            query.setParameter("receiver", receiver);
            query.setParameter("statusType", statusType);

            Set<UserInfo> result = query.list().stream()
                    .map(Relationship::getReceiver).collect(Collectors.toSet());
            result.addAll(query.list().stream()
                    .map(Relationship::getSender).collect(Collectors.toSet()));
            result.remove(receiver);

            return new ArrayList<>(result);
        }
    }

    @Override
    public List<UserInfo> getFriendRequestsOfUser(UserInfo user) {
        StatusType statusType = statusTypeRepository.getByType(PENDING.name());
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery(
                    "select r.sender from Relationship r where r.receiver = :user AND r.statusType = :statusType",
                    UserInfo.class);
            query.setParameter("user", user);
            query.setParameter("statusType", statusType);

            return query.list();
        }
    }

    @Override
    public List<UserInfo> getBlockList(UserInfo user) {
        StatusType statusType = statusTypeRepository.getByType(BLOCKED.name());
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery(
                    "select r.receiver from Relationship r where r.sender = :user AND r.statusType = :statusType",
                    UserInfo.class);
            query.setParameter("user", user);
            query.setParameter("statusType", statusType);

            return query.list();
        }
    }

    @Override
    public List<UserInfo> getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery("SELECT ui FROM UserInfo ui " +
                    "JOIN User u on ui.username = u.username WHERE u.enabled = true AND ui.username LIKE :username", UserInfo.class);
            query.setParameter("username", username);

            return query.list();
        }
    }

    @Override
    public List<UserInfo> getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery("SELECT ui FROM UserInfo ui " +
                    "JOIN User u on ui.username = u.username WHERE u.enabled = true AND ui.email LIKE :email", UserInfo.class);
            query.setParameter("email", email);

            return query.list();
        }
    }

    @Override
    public List<UserInfo> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("SELECT ui FROM UserInfo ui " +
                            "JOIN User u on ui.username = u.username WHERE u.enabled = true", UserInfo.class)
                    .list();
        }
    }

    @Override
    public List<Post> getAllPostFeed(UserInfo user) {
        List<UserInfo> friendsOfUser = getFriendsOfUser(user);
        List<Post> postList = new ArrayList<>();

        for (UserInfo u : friendsOfUser) {
            postList.addAll(postRepository.findAllByCreatedBy(u.getId()));
        }

        postList.addAll(postRepository.findAllByCreatedBy(user.getId()));

        return postList;
    }

    @Override
    public List<UserInfo> getAllUsersByFullName(String firstName, String lastName) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery("SELECT ui FROM UserInfo ui JOIN User u on ui.username = u.username " +
                    "WHERE u.enabled = true AND ui.firstName LIKE :firstName AND ui.lastName LIKE :lastName", UserInfo.class);
            query.setParameter("firstName", firstName);
            query.setParameter("lastName", lastName);

            return query.list();
        }
    }

    @Override
    public List<UserInfo> getAllPublicVisibleUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserInfo> query = session.createQuery("SELECT ui FROM UserInfo ui JOIN User u on ui.username = u.username" +
                    " WHERE u.enabled = true AND ui.profileVisibility.type = 'EVERYONE'", UserInfo.class);

            return query.list();
        }
    }

    @Override
    public UserInfo create(UserInfo user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public Relationship updateRelationship(Relationship relationship) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(relationship);
            session.getTransaction().commit();
            return relationship;
        }
    }

    @Override
    public UserInfo update(UserInfo user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public void changePassword(UserInfo userInfo, PasswordChangeDTO passwordDTO) {
        UserDetails userDetails = userDetailsManager.loadUserByUsername(userInfo.getUsername());
        userDetailsManager.deleteUser(userDetails.getUsername());

        org.springframework.security.core.userdetails.User newDetailsUser =
                new org.springframework.security.core.userdetails.User(
                        userInfo.getUsername(), passwordEncoder.encode(passwordDTO.getNewPassword()), userDetails.getAuthorities());

        userDetailsManager.createUser(newDetailsUser);
    }

    @Override
    public void delete(UUID id) {
        try (Session session = sessionFactory.openSession()) {
            UserInfo user = session.get(UserInfo.class, id);
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkUserExistsById(UUID id) {
        return getById(id) != null;
    }

    @Override
    public boolean checkUserExistsByUsername(String username) {
        return getByUsername(username).size() != 0;
    }

    @Override
    public boolean checkUserExistsByEmail(String email) {
        return getByEmail(email).size() != 0;
    }

    @Override
    public boolean checkRelationshipExists(UserInfo sender, UserInfo receiver, StatusType status) {
        try (Session session = sessionFactory.openSession()) {
            Query<Relationship> query = session.createQuery(
                    "SELECT r FROM Relationship r WHERE (r.sender = :sender AND r.receiver = :receiver AND r.statusType = :status) " +
                            "OR (r.sender = :receiver AND r.receiver = :sender AND r.statusType = :status)", Relationship.class);
            query.setParameter("sender", sender);
            query.setParameter("receiver", receiver);
            query.setParameter("status", status);

            return query.list().size() != 0;
        }
    }
}
