package com.assn.demo.repos;

import com.assn.demo.models.StatusType;
import java.util.List;

public interface StatusTypeRepository {

    StatusType getById(int id);

    StatusType getByType(String statusType);

    List<StatusType> getAll();
}
