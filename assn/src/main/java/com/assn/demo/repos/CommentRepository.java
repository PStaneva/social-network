package com.assn.demo.repos;

import com.assn.demo.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface CommentRepository extends JpaRepository<Comment, UUID> {

    List<Comment> findAllByOrderByCreatedAtAsc();

    @Query("SELECT c FROM Comment c WHERE c.post.id = :id ORDER BY c.createdAt DESC")
    List<Comment> findAllByPostOrderByCreatedAtDesc(@Param("id") UUID id);

    @Query("SELECT c FROM Comment c WHERE c.parentComment.id = :parentComment")
    List<Comment> findAllByParentComment(@Param("parentComment") UUID id);

    @Modifying
    @Query("DELETE FROM Comment c WHERE c.post.id = :postId")
    int deleteByPostId(@Param("postId") UUID postId);

    @Modifying
    @Query("DELETE FROM Comment c WHERE c.createdBy.id = :userId")
    int deleteByCreatedById(@Param("userId") UUID userId);
}