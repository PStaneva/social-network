package com.assn.demo.repos;

import com.assn.demo.models.Relationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface RelationshipRepository extends JpaRepository<Relationship, UUID> {

    Relationship getRelationshipByReceiverIdAndSenderId(UUID receiver, UUID sender);

    @Modifying
    @Query("DELETE FROM Relationship r WHERE r.sender.id = :userId OR r.receiver.id = :userId")
    int deleteByUser(@Param("userId") UUID userId);
}
