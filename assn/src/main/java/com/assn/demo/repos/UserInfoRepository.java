package com.assn.demo.repos;

import com.assn.demo.models.*;
import java.util.List;
import java.util.UUID;

public interface UserInfoRepository {

    UserInfo getById(UUID id);

    List<UserInfo> getFriendsOfUser(UserInfo receiver);

    List<UserInfo> getFriendRequestsOfUser(UserInfo user);

    List<UserInfo> getBlockList(UserInfo user);

    List<UserInfo> getByUsername(String username);

    List<UserInfo> getByEmail(String email);

    List<UserInfo> getAll();

    List<Post> getAllPostFeed(UserInfo user);

    List<UserInfo> getAllUsersByFullName(String firstName, String lastName);

    List<UserInfo> getAllPublicVisibleUsers();

    UserInfo create(UserInfo user);

    Relationship updateRelationship(Relationship relationship);

    UserInfo update(UserInfo user);

    void changePassword(UserInfo userInfo, PasswordChangeDTO passwordDTO);

    void delete(UUID id);

    boolean checkUserExistsById(UUID id);

    boolean checkUserExistsByUsername(String username);

    boolean checkUserExistsByEmail(String email);

    boolean checkRelationshipExists(UserInfo sender, UserInfo receiver, StatusType status);
}
