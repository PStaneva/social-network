package com.assn.demo.repos;

import com.assn.demo.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface PostRepository extends JpaRepository<Post, UUID> {

    List<Post> findAllByOrderByCreatedAtAsc();

    List<Post> findAllByOrderByCreatedAtDesc();

    @Query("SELECT p FROM Post p WHERE p.createdBy.id = :id")
    List<Post> findAllByCreatedBy(@Param("id") UUID id);

    @Query("SELECT p FROM Post p WHERE p.createdBy.profileVisibility.type = 'EVERYONE'")
    List<Post> findAllPublicVisibleUsersPosts();

    @Modifying
    @Query("UPDATE Post p SET p.likesCount = :count WHERE p.id = :postId")
    int setLikesCount(@Param("postId") UUID postId, @Param("count") int count);

    @Modifying
    @Query("DELETE FROM Post p WHERE p.createdBy.id = :userId")
    int deleteByCreatedById(@Param("userId") UUID userId);
}
