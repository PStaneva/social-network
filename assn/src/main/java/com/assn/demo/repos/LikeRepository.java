package com.assn.demo.repos;

import com.assn.demo.models.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@Transactional(readOnly = true)
public interface LikeRepository extends JpaRepository<Like, UUID> {

    @Query("SELECT l FROM Like l WHERE l.post.id = :postId AND l.userInfo.id = :userId AND l.isLiked = true")
    Optional<Like> findLikeByPostAndUser(@Param("postId") UUID postId, @Param("userId") UUID userId);

    @Query("SELECT l FROM Like l WHERE l.post.id = :postId AND l.isLiked = true")
    List<Like> findLikesByPost(@Param("postId") UUID postId);

    @Modifying
    @Query("DELETE FROM Like l WHERE l.post.id = :postId")
    int deleteByPostId(@Param("postId") UUID postId);

    @Modifying
    @Query("DELETE FROM Like l WHERE l.userInfo.id = :userId")
    int deleteByUserInfo_Id(@Param("userId") UUID userId);
}
