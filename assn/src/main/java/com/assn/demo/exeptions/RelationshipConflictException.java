package com.assn.demo.exeptions;

public class RelationshipConflictException extends RuntimeException {

    public RelationshipConflictException(String message) {
        super(message);
    }
}
