package com.assn.demo.exeptions;

public class InvalidUserOperation extends RuntimeException {

    public InvalidUserOperation(String message) {
        super(message);
    }
}
