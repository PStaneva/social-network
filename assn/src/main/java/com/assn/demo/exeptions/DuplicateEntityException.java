package com.assn.demo.exeptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String attribute, String value) {
        super(String.format("%s %s already exists.", attribute, value));
    }

    public DuplicateEntityException(String message) {
        super(message);
    }
}
