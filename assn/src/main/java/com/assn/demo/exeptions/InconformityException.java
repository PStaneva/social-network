package com.assn.demo.exeptions;

public class InconformityException extends RuntimeException {

    public InconformityException(String message) {
        super(message);
    }
}
