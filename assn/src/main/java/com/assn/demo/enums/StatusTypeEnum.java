package com.assn.demo.enums;

public enum StatusTypeEnum {
    PENDING,
    FRIENDS,
    BLOCKED
}
