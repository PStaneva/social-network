package com.assn.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssnApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssnApplication.class, args);
    }
}
