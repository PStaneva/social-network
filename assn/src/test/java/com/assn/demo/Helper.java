package com.assn.demo;

import com.assn.demo.models.*;

import java.time.LocalDateTime;

import static java.util.UUID.randomUUID;

public class Helper {

    public static UserInfo createUserInfo() {
        UserInfo user = new UserInfo();

        user.setId(randomUUID());
        user.setEmail("katrine@gmail.com");
        user.setUsername("Katrine");
        user.setFirstName("Mladenova");
        user.setLastName("Spassova");
        user.setProfileVisibility(toVisibility());

        return user;
    }

    public static UserInfoDTO createUserInfoDTO() {
        UserInfoDTO dto = new UserInfoDTO();

        dto.setEmail("katrine@gmail.com");
        dto.setUsername("Katrine");
        dto.setFirstName("Mladenova");
        dto.setLastName("Spassova");

        return dto;
    }

    public static UserRegistrationDTORest createUserRegistrationDTO() {
        UserRegistrationDTORest user = new UserRegistrationDTORest();

        user.setEmail("katrine@gmail.com");
        user.setUsername("Katrine");
        user.setFirstName("Mladenova");
        user.setLastName("Spassova");
        user.setPassword("123456Is");

        return user;
    }

    public static Comment toComment() {
        UserInfo user = createUserInfo();
        Post post = toPost();
        Comment comment = new Comment();

        comment.setId(randomUUID());
        comment.setText("Comment description.");
        comment.setCreatedAt(LocalDateTime.now());
        comment.setCreatedBy(user);
        comment.setPost(post);

        return comment;
    }

    public static Post toPost() {
        UserInfo user = createUserInfo();
        Post post = new Post();

        post.setId(randomUUID());
        post.setTitle("Title");
        post.setContent("Description...");
        post.setCreatedBy(user);

        return post;
    }

    public static ProfileVisibility toVisibility() {
        ProfileVisibility visibility = new ProfileVisibility();

        visibility.setId(randomUUID());
        visibility.setType("EVERYONE");

        return visibility;
    }

    public static StatusType toStatusType(String type) {
        StatusType statusType = new StatusType();

        statusType.setStatusId(1);
        statusType.setStatusType(type);

        return statusType;
    }

    public static Relationship toRelationship(UserInfo sender, UserInfo receiver, StatusType type) {
        Relationship relationship = new Relationship();

        relationship.setId(randomUUID());
        relationship.setSender(sender);
        relationship.setReceiver(receiver);
        relationship.setStatusType(type);

        return relationship;
    }

    public static VerificationToken toToken() {
        UserInfo user = createUserInfo();

        VerificationToken token = new VerificationToken();
        token.setId(randomUUID());
        token.setToken(randomUUID().toString());
        token.setUser(user);

        return token;
    }

    public static PasswordChangeDTO toPasswordChangeDTO() {
        PasswordChangeDTO passwordDTO = new PasswordChangeDTO();

        passwordDTO.setCurrentPassword("1245Pass");
        passwordDTO.setNewPassword("1245New_Pass");
        passwordDTO.setConfirmPassword("1245New_Pass");

        return passwordDTO;
    }
}
