package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.VerificationToken;
import com.assn.demo.repos.VerificationTokenRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.assn.demo.Helper.createUserInfo;
import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class VerificationTokenServiceImplTests {

    @Mock
    VerificationTokenRepository tokenRepository;

    @InjectMocks
    VerificationTokenServiceImpl tokenService;

    @Test
    public void getByIdShould_ReturnToken_WhenTokenExists() {
        // Arrange
        VerificationToken token = new VerificationToken();
        token.setToken(randomUUID().toString());
        token.setUser(createUserInfo());

        when(tokenRepository.findByToken(anyString()))
                .thenReturn(Optional.of(token));

        // Act
        VerificationToken returnedToken = tokenService.getVerificationToken(token.getToken());

        // Assert
        assertSame(token, returnedToken);
    }

    @Test
    public void getByTokenShould_Throw_WhenTokenNotExists() {
        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> tokenService.getVerificationToken(anyString()));
    }
}
