package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.InconformityException;
import com.assn.demo.models.Mapper;
import com.assn.demo.models.Post;
import com.assn.demo.models.PostDTO;
import com.assn.demo.models.UserInfo;
import com.assn.demo.repos.CommentRepository;
import com.assn.demo.repos.LikeRepository;
import com.assn.demo.repos.PostRepository;
import com.assn.demo.repos.UserInfoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.assn.demo.Helper.createUserInfo;
import static com.assn.demo.Helper.toPost;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PostServiceImplTests {

    @Mock
    PostRepository postRepository;

    @Mock
    UserInfoRepository userInfoRepository;

    @Mock
    CommentRepository commentRepository;

    @Mock
    LikeRepository likeRepository;

    @Spy
    Mapper mapper;

    @InjectMocks
    PostServiceImpl postService;

    @Test
    public void getByIdShould_ReturnPost_WhenPostExists() {
        // Arrange
        Post post = toPost();

        when(postRepository.findById(post.getId()))
                .thenReturn(java.util.Optional.of(post));

        // Act
        Post returnedPost = postService.getById(post.getId());

        // Assert
        assertSame(post, returnedPost);
    }

    @Test
    public void getByIdShould_Throw_WhenPostNotExists() {
        // Arrange
        Post post = toPost();

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> postService.getById(post.getId()));
    }

    @Test
    public void getAllPostsByUserShould_ReturnAllPostsByUser_WhenUserExists() {
        // Arrange
        Post post = toPost();
        List<Post> list = new ArrayList<>();
        list.add(post);

        when(userInfoRepository.checkUserExistsById(post.getCreatedBy().getId()))
                .thenReturn(true);

        when(postRepository.findAllByCreatedBy(post.getCreatedBy().getId()))
                .thenReturn(list);

        // Act
        List<Post> returnedPosts = postService.getAllPostsByUser(post.getCreatedBy().getId());

        // Assert
        assertSame(list, returnedPosts);
    }

    @Test
    public void getAllPublicVisibleUsersPostsShould_ReturnAllPostsCreatedByUsersWithPublicVisibility() {
        // Arrange
        Post post1 = toPost();
        Post post2 = toPost();
        List<Post> list = new ArrayList<>();
        list.add(post1);
        list.add(post2);

        when(postRepository.findAllPublicVisibleUsersPosts())
                .thenReturn(list);

        // Act
        List<Post> returnedPosts = postService.getAllPublicVisibleUsersPosts();

        // Assert
        assertSame(list, returnedPosts);
    }

    @Test
    public void getAllShould_ReturnAllPosts() {
        // Arrange
        Post post1 = toPost();
        Post post2 = toPost();
        List<Post> list = new ArrayList<>();
        list.add(post1);
        list.add(post2);

        when(postRepository.findAll())
                .thenReturn(list);

        // Act
        List<Post> returnedPosts = postService.getAll();

        // Assert
        assertSame(list, returnedPosts);
    }

    @Test
    public void createShould_CreatePost_WhenPostIsValid() {
        // Arrange
        UserInfo user = createUserInfo();
        PostDTO postDTO = new PostDTO();
        postDTO.setTitle("Testing post service layer");
        postDTO.setContent("Some description");

        // Act
        postService.create(postDTO, user);

        // Assert
        verify(postRepository, times(1))
                .save(any(Post.class));
    }

    @Test
    public void updateShould_UpdatePost_WhenPostExists() {
        // Arrange
        Post oldPost = toPost();
        PostDTO postDTO = new PostDTO();
        postDTO.setTitle("Testing post service layer");
        postDTO.setContent("Some description");

        when(postRepository.findById(oldPost.getId()))
                .thenReturn(Optional.of(oldPost));

        Post newPost = mapper.mergePosts(oldPost, postDTO);

        // Act
        postService.update(oldPost.getId(), postDTO, oldPost.getCreatedBy());

        // Assert
        ArgumentCaptor<Post> captor = forClass(Post.class);
        verify(postRepository).save(captor.capture());
        assertEquals(captor.getValue().getTitle(), newPost.getTitle());
    }

    @Test
    @WithMockUser(roles = "USER")
    public void updateShould_Throw_WhenPostCreatorIsNotTheSame() {
        // Arrange
        UserInfo user = createUserInfo();
        Post post = toPost();
        PostDTO postDTO = new PostDTO();
        postDTO.setTitle("Testing post service layer");
        postDTO.setContent("Some description");

        when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // Act, Assert
        assertThrows(InconformityException.class,
                () -> postService.update(post.getId(), postDTO, user));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void deleteShould_RemovePost_WhenPostExists() {
        // Arrange
        Post post = toPost();

        when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        when(commentRepository.deleteByPostId(post.getId()))
                .thenReturn(1);

        when(likeRepository.deleteByPostId(post.getId()))
                .thenReturn(1);

        // Act
        postService.delete(post.getId(), post.getCreatedBy());

        // Assert
        verify(postRepository, times(1))
                .delete(any(Post.class));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void deleteShould_Throw_WhenPostCreatorIsNotTheSame() {
        // Arrange
        UserInfo user = createUserInfo();
        Post post = toPost();

        when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // Act, Assert
        assertThrows(InconformityException.class,
                () -> postService.delete(post.getId(), user));
    }

    @Test
    public void showPostsSortedByDateAscShould_ReturnAllPostsSortedAsc() {
        // Arrange
        Post post1 = toPost();
        Post post2 = toPost();
        List<Post> list = new ArrayList<>();
        list.add(post1);
        list.add(post2);

        when(postRepository.findAllByOrderByCreatedAtAsc())
                .thenReturn(list);

        // Act
        List<Post> returnedPosts = postService.showPostsSortedByDateAsc();

        // Assert
        assertSame(list, returnedPosts);
    }

    @Test
    public void showPostsSortedByDateDescShould_ReturnAllPostsSortedDesc() {
        // Arrange
        Post post1 = toPost();
        Post post2 = toPost();
        List<Post> list = new ArrayList<>();
        list.add(post2);
        list.add(post1);

        when(postRepository.findAllByOrderByCreatedAtDesc())
                .thenReturn(list);

        // Act
        List<Post> returnedPosts = postService.showPostsSortedByDateDesc();

        // Assert
        assertSame(list, returnedPosts);
    }
}
