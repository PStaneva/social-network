package com.assn.demo.services;

import com.assn.demo.models.StatusType;
import com.assn.demo.repos.StatusTypeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.assn.demo.Helper.toStatusType;
import static com.assn.demo.enums.StatusTypeEnum.*;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class StatusTypeServiceImplTests {

    @Mock
    StatusTypeRepository statusTypeRepository;

    @InjectMocks
    StatusTypeServiceImpl statusTypeService;

    @Test
    public void getByIdShould_ReturnStatusType_WhenStatusTypeExists() {
        // Arrange
        StatusType statusType = toStatusType(FRIENDS.name());

        when(statusTypeRepository.getById(anyInt()))
                .thenReturn(statusType);

        // Act
        StatusType returnedStatusType = statusTypeService.getById(statusType.getStatusId());

        // Assert
        assertSame(statusType, returnedStatusType);
    }

    @Test
    public void getByTypeShould_ReturnVisibility_WhenVisibilityTypeExists() {
        // Arrange
        StatusType statusType = toStatusType(FRIENDS.name());

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(statusType);

        // Act
        StatusType returnedStatusType = statusTypeService.getByType(anyString());

        // Assert
        assertSame(statusType, returnedStatusType);
    }

    @Test
    public void getAllShould_ReturnAllStatusTypes() {
        // Arrange
        StatusType statusType1 = toStatusType(FRIENDS.name());
        StatusType statusType2 = toStatusType(PENDING.name());
        StatusType statusType3 = toStatusType(BLOCKED.name());

        List<StatusType> list = new ArrayList<>();
        list.add(statusType1);
        list.add(statusType2);
        list.add(statusType3);

        when(statusTypeRepository.getAll())
                .thenReturn(list);

        // Act
        List<StatusType> returnedList = statusTypeService.getAll();

        // Assert
        assertSame(list, returnedList);
    }
}
