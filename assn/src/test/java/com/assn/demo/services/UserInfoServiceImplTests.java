package com.assn.demo.services;

import com.assn.demo.exeptions.DuplicateEntityException;
import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.PasswordMismatchException;
import com.assn.demo.exeptions.RelationshipConflictException;
import com.assn.demo.models.*;
import com.assn.demo.repos.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.assn.demo.Helper.*;
import static com.assn.demo.enums.StatusTypeEnum.*;
import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserInfoServiceImplTests {

    @Mock
    UserInfoRepository userInfoRepository;
    @Mock
    ProfileVisibilityRepository visibilityRepository;
    @Mock
    StatusTypeRepository statusTypeRepository;
    @Mock
    RelationshipRepository relationshipRepository;
    @Mock
    PostRepository postRepository;
    @Mock
    LikeRepository likeRepository;
    @Mock
    CommentRepository commentRepository;
    @Mock
    VerificationTokenRepository tokenRepository;

    @Spy
    Principal principal;
    @Spy
    UserDetailsManager userDetailsManager;
    @Spy
    BCryptPasswordEncoder passwordEncoder;
    @Spy
    Mapper mapper;

    @InjectMocks
    UserInfoServiceImpl userInfoService;

    @Test
    public void getByIdShould_ReturnUser_WhenUserExists() {
        // Arrange
        UserInfo expectedUser = createUserInfo();
        UUID id = expectedUser.getId();

        when(userInfoRepository.getById(id))
                .thenReturn(expectedUser);

        // Act
        UserInfo returnedUser = userInfoService.getById(id);

        // Assert
        assertSame(returnedUser, expectedUser);
    }

    @Test
    public void getAllShould_ReturnAllUsers() {
        // Arrange
        UserInfo user_1 = createUserInfo();
        UserInfo user_2 = createUserInfo();

        List<UserInfo> users = new ArrayList<>();
        users.add(user_1);
        users.add(user_2);

        when(userInfoRepository.getAll())
                .thenReturn(users);

        // Act
        List<UserInfo> returnedUsers = userInfoService.getAll();

        // Assert
        assertSame(returnedUsers, users);
    }

    @Test
    public void getAllUsersByFullNameShould_ReturnAllUsersWithTheSameName() {
        // Arrange
        UserInfo user_1 = createUserInfo();
        UserInfo user_2 = createUserInfo();

        List<UserInfo> users = new ArrayList<>();
        users.add(user_1);
        users.add(user_2);

        when(userInfoRepository.getAllUsersByFullName(anyString(), anyString()))
                .thenReturn(users);

        // Act
        List<UserInfo> returnedUsers = userInfoService.getAllUsersByFullName(anyString(), anyString());

        // Assert
        assertSame(returnedUsers, users);
    }

    @Test
    public void getFriendsOfUserShould_ReturnAllFriends() {
        // Arrange
        UserInfo sender = createUserInfo();
        UserInfo receiver = createUserInfo();

        List<UserInfo> friends = new ArrayList<>();
        friends.add(sender);
        friends.add(receiver);

        when(userInfoRepository.getById(receiver.getId()))
                .thenReturn(receiver);

        when(userInfoRepository.getFriendsOfUser(receiver))
                .thenReturn(friends);

        // Act
        List<UserInfo> returnedFriends = userInfoService.getFriendsOfUser(receiver.getId());

        // Assert
        assertSame(returnedFriends, friends);
    }

    @Test
    public void getFriendRequestsOfUserShould_ReturnAllRequests() {
        // Arrange
        UserInfo user = createUserInfo();
        UserInfo friend_1 = createUserInfo();
        UserInfo friend_2 = createUserInfo();

        List<UserInfo> requests = new ArrayList<>();
        requests.add(friend_1);
        requests.add(friend_2);

        when(userInfoRepository.getById(user.getId()))
                .thenReturn(user);

        when(userInfoRepository.getFriendRequestsOfUser(user))
                .thenReturn(requests);

        // Act
        List<UserInfo> returnedFriends = userInfoService.getFriendRequestsOfUser(user.getId());

        // Assert
        assertSame(returnedFriends, requests);
    }

    @Test
    public void getBlockListShould_ReturnAllBlockedUsers() {
        // Arrange
        UserInfo user = createUserInfo();
        UserInfo blockedUser_1 = createUserInfo();
        UserInfo blockedUser_2 = createUserInfo();

        List<UserInfo> blockList = new ArrayList<>();
        blockList.add(blockedUser_1);
        blockList.add(blockedUser_2);

        when(userInfoRepository.getById(user.getId()))
                .thenReturn(user);

        when(userInfoRepository.getBlockList(user))
                .thenReturn(blockList);

        // Act
        List<UserInfo> returnedFriends = userInfoService.getBlockList(user.getId());

        // Assert
        assertSame(returnedFriends, blockList);
    }

    @Test
    public void getByUsernameShould_ReturnUser_WhenUsernameIsValid() {
        // Arrange
        UserInfo user = createUserInfo();
        String username = user.getUsername();
        List<UserInfo> users = new ArrayList<>();
        users.add(user);

        when(userInfoRepository.checkUserExistsByUsername(anyString()))
                .thenReturn(true);

        when(userInfoRepository.getByUsername(anyString()))
                .thenReturn(users);

        // Act
        UserInfo returnedUser = userInfoService.getByUsername(username);

        // Assert
        assertSame(returnedUser, user);
    }

    @Test
    public void getByUsernameShould_Throw_WhenUsernameIsNotValid() {
        // Arrange
        when(userInfoRepository.checkUserExistsByUsername(anyString()))
                .thenReturn(false);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userInfoService.getByUsername(anyString()));
    }

    @Test
    public void getByEmailShould_ReturnUser_WhenEmailIsValid() {
        // Arrange
        UserInfo user = createUserInfo();
        String email = user.getEmail();
        List<UserInfo> users = new ArrayList<>();
        users.add(user);

        when(userInfoRepository.checkUserExistsByEmail(anyString()))
                .thenReturn(true);

        when(userInfoRepository.getByEmail(anyString()))
                .thenReturn(users);

        // Act
        UserInfo returnedUser = userInfoService.getByEmail(email);

        // Assert
        assertSame(returnedUser, user);
    }


    @Test
    public void getByEmailShould_Throw_WhenEmailIsNotValid() {
        // Arrange
        when(userInfoRepository.checkUserExistsByEmail(anyString()))
                .thenReturn(false);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userInfoService.getByEmail(anyString()));
    }

    @Test
    public void setUserEnabledShould_MakeUserValid() {
        // Arrange
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User user =
                new org.springframework.security.core.userdetails.User(
                        "Elizabet", passwordEncoder.encode(anyString()), authorities);
        userDetailsManager.createUser(user);

        when(userDetailsManager.loadUserByUsername(anyString()))
                .thenReturn(user);

        // Act
        userInfoService.setUserEnabled(user.getUsername(), false);

        // Assert
        ArgumentCaptor<UserDetails> captor = forClass(UserDetails.class);
        verify(userDetailsManager).updateUser(captor.capture());
        assertFalse(captor.getValue().isEnabled());
    }

    @Test
    public void emailConfirmationByRegistrationShould_ConfirmUserRegistration_WhenTokenIsValid() {
        // Arrange
        VerificationToken token = toToken();

        when(tokenRepository.findByToken(anyString()))
                .thenReturn(java.util.Optional.of(token));

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User user =
                new org.springframework.security.core.userdetails.User(
                        "Elizabet", passwordEncoder.encode(anyString()),
                        false, true, true, true, authorities);

        userDetailsManager.createUser(user);

        when(userDetailsManager.loadUserByUsername(anyString()))
                .thenReturn(user);

        // Act
        UserInfo expectedUser = userInfoService.emailConfirmationByRegistration(anyString());

        // Assert
        verify(tokenRepository, times(1))
                .delete(any(VerificationToken.class));
    }

    @Test
    public void emailConfirmationByRegistrationShould_Throw_WhenTokenIsExpired() throws ParseException {
        // Arrange
        String startDate = "04-28-2020";
        SimpleDateFormat sd = new SimpleDateFormat("MM-dd-yyyy");
        java.util.Date utilDate = sd.parse(startDate);
        java.sql.Date date = new java.sql.Date(utilDate.getTime());

        VerificationToken token = toToken();
        token.setExpiryDate(date);

        when(tokenRepository.findByToken(anyString()))
                .thenReturn(java.util.Optional.of(token));

        when(userInfoRepository.getById(token.getUser().getId()))
                .thenReturn(token.getUser());

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userInfoService.emailConfirmationByRegistration(token.getToken()));
    }

    @Test
    public void getUserInfoByVisibilityShould_ReturnUserWithAllPossibleFields_WhenVisibilityIsEveryone() {
        // Arrange
        UserInfo principal = createUserInfo();
        UserInfo user = createUserInfo();
        user.setJob("Java Developer Intern");

        UserVisibilityDTO expectedUser = mapper.toUserVisibilityDTOWithAdditionalFields(user);
        StatusType type = toStatusType(FRIENDS.name());

        when(userInfoRepository.getById(user.getId()))
                .thenReturn(user);

        when(statusTypeRepository.getByType(FRIENDS.name()))
                .thenReturn(type);

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // Act
        UserVisibilityDTO returnedUser = userInfoService.getUserInfoByVisibility(user.getId(), principal);

        // Assert
        assertSame(returnedUser.getJob(), expectedUser.getJob());
    }

    @Test
    public void getAllPublicVisibleUsersShould_ReturnAllUserWithPublicVisibility() {
        // Arrange
        UserInfo user_1 = createUserInfo();
        UserInfo user_2 = createUserInfo();

        List<UserInfo> expectedList = new ArrayList<>();
        expectedList.add(user_1);
        expectedList.add(user_2);

        when(userInfoRepository.getAllPublicVisibleUsers())
                .thenReturn(expectedList);

        // Act
        List<UserInfo> returnedList = userInfoService.getAllPublicVisibleUsers();

        // Assert
        assertSame(returnedList, expectedList);
    }

    @Test
    public void createShould_CreateUser_WhenUserIsValid() {
        // Arrange
        UserRegistrationDTORest user = createUserRegistrationDTO();
        UserInfo userToCreate = mapper.toUserInfo(user);
        ProfileVisibility visibility = toVisibility();

        when(userInfoRepository.checkUserExistsByUsername(anyString()))
                .thenReturn(false);

        when(userInfoRepository.checkUserExistsByEmail(anyString()))
                .thenReturn(false);

        when(visibilityRepository.findByType(anyString()))
                .thenReturn(java.util.Optional.of(visibility));

        // Act
        userInfoService.create(user);

        // Assert
        ArgumentCaptor<UserInfo> captor = forClass(UserInfo.class);
        verify(userInfoRepository).create(captor.capture());
        assertEquals(captor.getValue().getUsername(), userToCreate.getUsername());
    }

    @Test
    public void createShould_Throw_WhenEmailIsDuplicated() {
        // Arrange
        UserRegistrationDTORest user = createUserRegistrationDTO();

        when(userInfoRepository.checkUserExistsByEmail(anyString()))
                .thenReturn(true);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> userInfoService.create(user));
    }

    @Test
    public void createShould_Throw_WhenUsernameIsDuplicated() {
        // Arrange
        UserRegistrationDTORest user = createUserRegistrationDTO();

        when(userInfoRepository.checkUserExistsByUsername(anyString()))
                .thenReturn(true);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> userInfoService.create(user));
    }

    @Test
    public void requestFriendshipShould_SendRequestToUser_WhenUserIsValid() {
        // Arrange
        UserInfo sender = createUserInfo();
        sender.setUsername("Johanna");
        UserInfo receiver = createUserInfo();
        StatusType type = toStatusType(PENDING.name());
        Relationship relationship = mapper.toRelationship(sender, receiver, type);

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(userInfoRepository.getById(receiver.getId()))
                .thenReturn(receiver);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(type);

        when(userInfoRepository.checkRelationshipExists(sender, receiver, type))
                .thenReturn(false);

        // Act
        userInfoService.requestFriendship(sender.getId(), receiver.getId());

        // Assert
        ArgumentCaptor<Relationship> captor = forClass(Relationship.class);
        verify(relationshipRepository).save(captor.capture());
        assertEquals(captor.getValue().getStatusType(), relationship.getStatusType());
    }

    @Test
    public void requestFriendshipShould_Throw_WhenUserSendRequestToHimself() {
        // Arrange
        UserInfo sender = createUserInfo();
        UserInfo receiver = createUserInfo();
        StatusType type = toStatusType(PENDING.name());

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(userInfoRepository.getById(receiver.getId()))
                .thenReturn(receiver);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(type);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> userInfoService.requestFriendship(sender.getId(), receiver.getId()));
    }

    @Test
    public void requestFriendshipShould_Throw_WhenUsersAreFriends() {
        // Arrange
        UserInfo sender = createUserInfo();
        sender.setUsername("Johanna");
        UserInfo receiver = createUserInfo();
        StatusType type = toStatusType(PENDING.name());

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(userInfoRepository.getById(receiver.getId()))
                .thenReturn(receiver);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(type);

        when(userInfoRepository.checkRelationshipExists(sender, receiver, type))
                .thenReturn(true);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> userInfoService.requestFriendship(sender.getId(), receiver.getId()));
    }

    @Test
    public void acceptFriendshipShould_AcceptRequest_WhenUsersAreValid() {
        // Arrange
        UserInfo anotherSender = createUserInfo();
        anotherSender.setUsername("Johanna");
        UserInfo sender = createUserInfo();
        sender.setUsername("Emma");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());
        StatusType typeFriends = toStatusType(FRIENDS.name());

        Relationship expectedRelationship = toRelationship(sender, receiver, typePending);
        Relationship relationship_2 = toRelationship(anotherSender, receiver, typeFriends);

        List<Relationship> list = new ArrayList<>();
        list.add(expectedRelationship);
        list.add(relationship_2);

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(statusTypeRepository.getByType(PENDING.name()))
                .thenReturn(typePending);

        when(relationshipRepository.findAll())
                .thenReturn(list);

        when(statusTypeRepository.getByType(FRIENDS.name()))
                .thenReturn(typeFriends);

        // Act
        userInfoService.acceptFriendship(sender.getId(), receiver);

        // Assert
        ArgumentCaptor<Relationship> captor = forClass(Relationship.class);
        verify(userInfoRepository).updateRelationship(captor.capture());
        assertEquals(captor.getValue(), expectedRelationship);
    }

    @Test
    public void acceptFriendshipShould_Throw_WhenUserEnterHisOwnId() {
        // Arrange
        UserInfo sender = createUserInfo();
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(typePending);

        // Act, Assert
        assertThrows(RelationshipConflictException.class,
                () -> userInfoService.acceptFriendship(sender.getId(), receiver));
    }

    @Test
    public void acceptFriendshipShould_Throw_WhenNoRequestExists() {
        // Arrange
        UserInfo sender = createUserInfo();
        sender.setUsername("Emma");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(typePending);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userInfoService.acceptFriendship(sender.getId(), receiver));
    }

    @Test
    public void blockFriendShould_BlockUser_WhenUserIsValid() {
        // Arrange
        UserInfo userToBeBlocked = createUserInfo();
        userToBeBlocked.setUsername("Emma");
        UserInfo sender = createUserInfo();
        StatusType typeFriends = toStatusType(FRIENDS.name());
        StatusType typeBlocked = toStatusType(BLOCKED.name());

        when(userInfoRepository.getById(userToBeBlocked.getId()))
                .thenReturn(userToBeBlocked);

        when(statusTypeRepository.getByType(FRIENDS.name()))
                .thenReturn(typeFriends);

        when(statusTypeRepository.getByType(BLOCKED.name()))
                .thenReturn(typeBlocked);

        Relationship relationship = toRelationship(sender, userToBeBlocked, typeFriends);

        List<Relationship> list = new ArrayList<>();
        list.add(relationship);

        when(relationshipRepository.findAll())
                .thenReturn(list);

        // Act
        userInfoService.blockFriend(userToBeBlocked.getId(), sender);

        // Assert
        Relationship expectedRelationship = mapper.toRelationship(sender, userToBeBlocked, typeBlocked);
        ArgumentCaptor<Relationship> captor = forClass(Relationship.class);
        verify(userInfoRepository).updateRelationship(captor.capture());
        assertEquals(expectedRelationship.getStatusType(), captor.getValue().getStatusType());
        assertEquals(expectedRelationship.getSender(), captor.getValue().getSender());
    }

    @Test
    public void blockFriendShould_Throw_WhenUserBlocksHimself() {
        // Arrange
        UserInfo user = createUserInfo();

        when(userInfoRepository.getById(user.getId()))
                .thenReturn(user);

        // Act, Assert
        assertThrows(RelationshipConflictException.class,
                () -> userInfoService.blockFriend(user.getId(), user));
    }

    @Test
    public void blockFriendShould_Throw_WhenExistingRelationshipNotFound() {
        // Arrange
        UserInfo userToBeBlocked = createUserInfo();
        userToBeBlocked.setUsername("Elizabet");
        UserInfo sender = createUserInfo();
        StatusType typeFriends = toStatusType(FRIENDS.name());

        when(userInfoRepository.getById(userToBeBlocked.getId()))
                .thenReturn(userToBeBlocked);

        when(statusTypeRepository.getByType(FRIENDS.name()))
                .thenReturn(typeFriends);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userInfoService.blockFriend(userToBeBlocked.getId(), sender));
    }

    @Test
    public void updateBasicUserInfoShould_UpdateUser_WhenUserIsValid() {
        // Arrange
        UserInfo oldUser = createUserInfo();
        UserInfo userToBeAuthenticated = createUserInfo();
        UserInfoDTO dto = createUserInfoDTO();
        UserInfo newUser = mapper.mergeUserInfos(oldUser, dto);

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newDetailsUser =
                new org.springframework.security.core.userdetails.User(
                        oldUser.getUsername(), passwordEncoder.encode(anyString()), authorities);
        userDetailsManager.createUser(newDetailsUser);

        when(userInfoRepository.getById(oldUser.getId()))
                .thenReturn(oldUser);

        when(userInfoRepository.checkUserExistsByUsername(anyString()))
                .thenReturn(false);

        when(userInfoRepository.checkUserExistsByEmail(anyString()))
                .thenReturn(false);

        when(userDetailsManager.loadUserByUsername(oldUser.getUsername()))
                .thenReturn(newDetailsUser);

        // Act
        userInfoService.updateBasicUserInfo(oldUser.getId(), dto, userToBeAuthenticated);

        // Assert
        ArgumentCaptor<UserInfo> captor = forClass(UserInfo.class);
        verify(userInfoRepository).update(captor.capture());
        assertEquals(captor.getValue(), newUser);
    }

    @Test
    public void updateBasicUserInfoShould_Throw_WhenUsernameIsDuplicated() {
        // Arrange
        UserInfo userToBeAuthenticated = createUserInfo();
        UserInfo oldUser = createUserInfo();
        UserInfoDTO dto = createUserInfoDTO();
        dto.setUsername("Anna");

        when(userInfoRepository.getById(oldUser.getId()))
                .thenReturn(oldUser);

        when(userInfoRepository.checkUserExistsByUsername(anyString()))
                .thenReturn(true);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> userInfoService.updateBasicUserInfo(oldUser.getId(), dto, userToBeAuthenticated));
    }

    @Test
    public void updateBasicUserInfoShould_Throw_WhenEmailIsDuplicated() {
        // Arrange
        UserInfo userToBeAuthenticated = createUserInfo();
        UserInfo oldUser = createUserInfo();
        UserInfoDTO dto = createUserInfoDTO();
        dto.setEmail("a.spasova@gmail.com");

        when(userInfoRepository.getById(oldUser.getId()))
                .thenReturn(oldUser);

        when(userInfoRepository.checkUserExistsByUsername(anyString()))
                .thenReturn(false);

        when(userInfoRepository.checkUserExistsByEmail(anyString()))
                .thenReturn(true);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> userInfoService.updateBasicUserInfo(oldUser.getId(), dto, userToBeAuthenticated));
    }

    @Test
    public void updateBasicUserInfoShould_Throw_WhenUserUpdateOtherProfileInfo() {
        // Arrange
        UserInfo userToBeAuthenticated = createUserInfo();
        userToBeAuthenticated.setUsername("Klara");
        UserInfo oldUser = createUserInfo();
        UserInfoDTO dto = createUserInfoDTO();

        when(userInfoRepository.getById(oldUser.getId()))
                .thenReturn(oldUser);

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // Act, Assert
        assertThrows(ResponseStatusException.class,
                () -> userInfoService.updateBasicUserInfo(oldUser.getId(), dto, userToBeAuthenticated));
    }

    @Test
    public void changePasswordShould_ChangeUserPassword_WhenUserIsValid() {
        // Arrange
        UserInfo userToBeAuthenticated = createUserInfo();
        UserInfo userInfo = createUserInfo();
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newDetailsUser =
                new org.springframework.security.core.userdetails.User(
                        userInfo.getUsername(), passwordEncoder.encode("1245Pass"), authorities);
        userDetailsManager.createUser(newDetailsUser);

        PasswordChangeDTO passwordDTO = toPasswordChangeDTO();

        when(userDetailsManager.loadUserByUsername(anyString()))
                .thenReturn(newDetailsUser);

        // Act
        userInfoService.changePassword(userInfo, passwordDTO, userToBeAuthenticated);

        // Assert
        verify(userInfoRepository, times(1))
                .changePassword(userInfo, passwordDTO);
    }

    @Test
    public void changePasswordShould_Throw_WhenPasswordsMismatch() {
        // Arrange
        UserInfo userToBeAuthenticated = createUserInfo();
        UserInfo userInfo = createUserInfo();
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newDetailsUser =
                new org.springframework.security.core.userdetails.User(
                        userInfo.getUsername(), passwordEncoder.encode(anyString()), authorities);
        userDetailsManager.createUser(newDetailsUser);

        PasswordChangeDTO passwordDTO = toPasswordChangeDTO();
        passwordDTO.setConfirmPassword("1245New_Pass_Mismatch");

        when(userDetailsManager.loadUserByUsername(anyString()))
                .thenReturn(newDetailsUser);

        // Act, Assert
        assertThrows(PasswordMismatchException.class,
                () -> userInfoService.changePassword(userInfo, passwordDTO, userToBeAuthenticated));
    }

    @Test
    public void changePasswordShould_Throw_WhenConfirmPasswordIsNotValid() {
        // Arrange
        UserInfo userToBeAuthenticated = createUserInfo();
        UserInfo userInfo = createUserInfo();
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newDetailsUser =
                new org.springframework.security.core.userdetails.User(
                        userInfo.getUsername(), passwordEncoder.encode("1245Pass"), authorities);
        userDetailsManager.createUser(newDetailsUser);

        PasswordChangeDTO passwordDTO = toPasswordChangeDTO();
        passwordDTO.setConfirmPassword("1245New_Pass_Mismatch");

        when(userDetailsManager.loadUserByUsername(anyString()))
                .thenReturn(newDetailsUser);

        // Act, Assert
        assertThrows(PasswordMismatchException.class,
                () -> userInfoService.changePassword(userInfo, passwordDTO, userToBeAuthenticated));
    }

    @Test
    public void changePasswordShould_Throw_WhenUserTryToChangePasswordOfAnotherUser() {
        // Arrange
        UserInfo userToBeAuthenticated = createUserInfo();
        userToBeAuthenticated.setUsername("Anna");
        UserInfo userInfo = createUserInfo();

        PasswordChangeDTO passwordDTO = toPasswordChangeDTO();

        // Act, Assert
        assertThrows(ResponseStatusException.class,
                () -> userInfoService.changePassword(userInfo, passwordDTO, userToBeAuthenticated));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void deleteShould_RemoveUser_WhenUserExists() {
        // Arrange
        UserInfo userToBeRemoved = createUserInfo();
        UserInfo user = createUserInfo();

        when(userInfoRepository.getById(userToBeRemoved.getId()))
                .thenReturn(userToBeRemoved);

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // Act
        userInfoService.delete(userToBeRemoved.getId(), user);

        // Assert
        verify(userInfoRepository, times(1))
                .delete(userToBeRemoved.getId());
    }

    @Test
    public void deleteShould_Throw_WhenUserTryToDeleteOtherUser() {
        // Arrange
        UserInfo user = createUserInfo();
        user.setUsername("Elizabet");
        UserInfo userToBeRemoved = createUserInfo();
        UUID id = userToBeRemoved.getId();

        when(userInfoRepository.getById(id))
                .thenReturn(userToBeRemoved);


        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // Act, Assert
        assertThrows(ResponseStatusException.class,
                () -> userInfoService.delete(id, user));
    }

    @Test
    public void unfriendUserShould_RemoveUserFromFriendList_WhenFriendIsValid() {
        // Arrange
        UserInfo anotherSender = createUserInfo();
        anotherSender.setUsername("Marina");
        UserInfo sender = createUserInfo();
        sender.setUsername("Ivana");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());
        StatusType typeFriends = toStatusType(FRIENDS.name());

        Relationship expectedRelationship = toRelationship(sender, receiver, typeFriends);
        Relationship relationship_2 = toRelationship(anotherSender, receiver, typePending);

        List<Relationship> list = new ArrayList<>();
        list.add(expectedRelationship);
        list.add(relationship_2);

        when(statusTypeRepository.getByType(FRIENDS.name()))
                .thenReturn(typeFriends);

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(relationshipRepository.findAll())
                .thenReturn(list);

        // Act
        userInfoService.unfriendUser(sender.getId(), receiver);

        // Assert
        verify(relationshipRepository, times(1))
                .delete(any(Relationship.class));
    }

    @Test
    public void unfriendUserShould_Throw_WhenUserTryToUnfriendHimself() {
        // Arrange
        UserInfo sender = createUserInfo();
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(FRIENDS.name());

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(typePending);

        // Act, Assert
        assertThrows(RelationshipConflictException.class,
                () -> userInfoService.unfriendUser(sender.getId(), receiver));
    }

    @Test
    public void unfriendUserShould_Throw_WhenNoFriendshipExists() {
        // Arrange
        UserInfo sender = createUserInfo();
        sender.setUsername("Marina");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(FRIENDS.name());

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(typePending);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userInfoService.unfriendUser(sender.getId(), receiver));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void declineFriendshipShould_DeclineRequest_WhenConnectionExists() {
        // Arrange
        UserInfo anotherSender = createUserInfo();
        anotherSender.setUsername("Marina");
        UserInfo sender = createUserInfo();
        sender.setUsername("Ivana");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());
        StatusType typeFriends = toStatusType(FRIENDS.name());

        Relationship expectedRelationship = toRelationship(sender, receiver, typePending);
        Relationship relationship_2 = toRelationship(anotherSender, receiver, typeFriends);

        List<Relationship> list = new ArrayList<>();
        list.add(expectedRelationship);
        list.add(relationship_2);

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(statusTypeRepository.getByType(PENDING.name()))
                .thenReturn(typePending);

        when(relationshipRepository.findAll())
                .thenReturn(list);

        // Act
        userInfoService.declineFriendship(sender.getId(), receiver);

        // Assert
        verify(relationshipRepository, times(1))
                .delete(any(Relationship.class));
    }

    @Test
    public void declineFriendshipShould_Throw_WhenUserTryToDeclineHimself() {
        // Arrange
        UserInfo sender = createUserInfo();
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(typePending);

        // Act, Assert
        assertThrows(RelationshipConflictException.class,
                () -> userInfoService.declineFriendship(sender.getId(), receiver));
    }

    @Test
    public void declineFriendshipShould_Throw_WhenNoRequestExists() {
        // Arrange
        UserInfo sender = createUserInfo();
        sender.setUsername("Marina");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());

        when(userInfoRepository.getById(sender.getId()))
                .thenReturn(sender);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(typePending);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userInfoService.declineFriendship(sender.getId(), receiver));
    }

    @Test
    public void unblockFriendShould_DeleteBlockedFriend_WhenConnectionExists() {
        UserInfo anotherSender = createUserInfo();
        anotherSender.setUsername("Nikol");
        UserInfo userToBeUnblocked = createUserInfo();
        UserInfo sender = createUserInfo();
        sender.setUsername("Johanna");
        StatusType typeBlocked = toStatusType(BLOCKED.name());
        StatusType typePending = toStatusType(PENDING.name());

        Relationship expectedRelationship = toRelationship(sender, userToBeUnblocked, typeBlocked);
        Relationship relationship_2 = toRelationship(anotherSender, userToBeUnblocked, typePending);

        List<Relationship> list = new ArrayList<>();
        list.add(expectedRelationship);
        list.add(relationship_2);

        when(userInfoRepository.getById(userToBeUnblocked.getId()))
                .thenReturn(userToBeUnblocked);

        when(statusTypeRepository.getByType(BLOCKED.name()))
                .thenReturn(typeBlocked);

        when(relationshipRepository.findAll())
                .thenReturn(list);

        // Act
        userInfoService.unblockFriend(userToBeUnblocked.getId(), sender);

        // Assert
        verify(relationshipRepository, times(1))
                .delete(any(Relationship.class));
    }

    @Test
    public void unblockFriendShould_Throw_WhenUserTryToUnblockHimself() {
        // Arrange
        UserInfo user = createUserInfo();

        when(userInfoRepository.getById(user.getId()))
                .thenReturn(user);

        // Act, Assert
        assertThrows(RelationshipConflictException.class,
                () -> userInfoService.unblockFriend(user.getId(), user));
    }

    @Test
    public void unblockFriendShould_Throw_WhenNoRequestExists() {
        // Arrange
        UserInfo userToBeUnblocked = createUserInfo();
        userToBeUnblocked.setUsername("Marina");
        UserInfo user = createUserInfo();
        StatusType type = toStatusType(BLOCKED.name());

        when(userInfoRepository.getById(userToBeUnblocked.getId()))
                .thenReturn(userToBeUnblocked);

        when(statusTypeRepository.getByType(anyString()))
                .thenReturn(type);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> userInfoService.unblockFriend(userToBeUnblocked.getId(), user));
    }

    @Test
    public void createVerificationTokenShould_CreateToken_WhenUserIsValid() {
        // Arrange
        UserInfo user = createUserInfo();

        // Act
        userInfoService.createVerificationToken(user, randomUUID().toString());

        // Assert
        verify(tokenRepository, times(1)).save(any(VerificationToken.class));
    }
}
