package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.exeptions.InconformityException;
import com.assn.demo.models.*;
import com.assn.demo.repos.CommentRepository;
import com.assn.demo.repos.PostRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.assn.demo.Helper.*;
import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTests {

    @Mock
    CommentRepository commentRepository;

    @Mock
    PostRepository postRepository;

    @Spy
    Mapper mapper;

    @InjectMocks
    CommentServiceImpl commentService;

    @Test
    public void getByIdShould_ReturnComment_WhenCommentExists() {
        // Arrange
        Comment expectedComment = toComment();
        UUID id = expectedComment.getId();

        when(commentRepository.findById(id))
                .thenReturn(Optional.of(expectedComment));

        // Act
        Comment returnedComment = commentService.getById(id);

        // Assert
        assertSame(returnedComment, expectedComment);
    }

    @Test
    public void getAllCommentsByPostShould_ReturnAllComments_WhenCommentsExist() {
        // Arrange
        Comment expectedComment_1 = toComment();
        Comment expectedComment_2 = toComment();
        List<Comment> commentList = new ArrayList<>();
        commentList.add(expectedComment_1);
        commentList.add(expectedComment_2);

        UUID postId = randomUUID();
        Post post = toPost();
        when(postRepository.existsById(postId))
                .thenReturn(true);

        expectedComment_1.setPost(post);
        expectedComment_2.setPost(post);
        when(commentRepository.findAllByPostOrderByCreatedAtDesc(postId))
                .thenReturn(commentList);

        // Act
        List<Comment> returnedComments = commentService.getAllCommentsByPost(postId);

        // Assert
        assertSame(returnedComments, commentList);
    }

    @Test
    public void getAllCommentsByPostShould_Throw_WhenPostNotExists() {
        // Arrange
        UUID postId = randomUUID();

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> commentService.getAllCommentsByPost(postId));
    }

    @Test
    public void getAllSortedChronologicallyShould_ReturnAllComments_WhenCommentsExist() {
        // Arrange
        Comment expectedComment_1 = toComment();
        Comment expectedComment_2 = toComment();
        List<Comment> commentList = new ArrayList<>();
        commentList.add(expectedComment_1);
        commentList.add(expectedComment_2);

        when(commentRepository.findAllByOrderByCreatedAtAsc())
                .thenReturn(commentList);

        // Act
        List<Comment> returnedComments = commentService.getAllSortedChronologically();

        // Assert
        assertSame(returnedComments, commentList);
    }

    @Test
    public void createShould_CreateComment_WhenCommentIsValid() {
        // Arrange
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setText("Description...");

        UUID postId = randomUUID();
        Post post = toPost();
        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));

        UserInfo user = createUserInfo();

        // Act
        commentService.create(postId, commentDTO, user);

        // Assert
        verify(commentRepository, times(1))
                .save(any(Comment.class));
    }

    @Test
    public void updateShould_UpdateComment_WhenCommentIsValid() {
        // Arrange
        Post post = toPost();
        UserInfo user = createUserInfo();

        UUID id = randomUUID();
        Comment oldComment = new Comment();
        oldComment.setId(id);
        oldComment.setPost(post);
        oldComment.setCreatedBy(user);

        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setText("New Description...");

        Comment comment = mapper.mergeComments(oldComment, commentDTO);

        when(commentRepository.findById(id))
                .thenReturn(Optional.of(oldComment));

        // Act
        commentService.update(id, commentDTO, user);

        // Assert
        ArgumentCaptor<Comment> captor = forClass(Comment.class);
        verify(commentRepository).save(captor.capture());
        assertEquals(captor.getValue().getId(), comment.getId());
    }

    @Test
    @WithMockUser(roles = "USER")
    public void updateShould_Throw_WhenCreatedByIsNotValid() {
        // Arrange
        Post post = toPost();
        UserInfo user = createUserInfo();
        UserInfo fakeUser = createUserInfo();
        fakeUser.setUsername("Fake");

        UUID id = randomUUID();
        Comment oldComment = new Comment();
        oldComment.setId(id);
        oldComment.setPost(post);
        oldComment.setCreatedBy(fakeUser);

        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setText("Description...");

        when(commentRepository.findById(id))
                .thenReturn(Optional.of(oldComment));

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // Act, Assert
        assertThrows(InconformityException.class,
                () -> commentService.update(id, commentDTO, user));
    }

    @Test
    public void deleteShould_DeleteComment_WhenCommentIdIsValid() {
        // Arrange
        Post post = toPost();
        UserInfo user = createUserInfo();

        UUID id = randomUUID();
        Comment comment = new Comment();
        comment.setId(id);
        comment.setPost(post);
        comment.setCreatedBy(user);

        List<Comment> comments = new ArrayList<>();
        comments.add(comment);

        when(commentRepository.findById(id))
                .thenReturn(Optional.of(comment));

        when(commentRepository.findAllByParentComment(id))
                .thenReturn(comments);

        // Act
        commentService.delete(id, user);

        // Assert
        verify(commentRepository, times(1)).delete(comment);
    }

    @Test
    @WithMockUser(roles = "USER")
    public void deleteShould_Throw_WhenCreatedByIsNotValid() {
        // Arrange
        Post post = toPost();
        UserInfo user = createUserInfo();
        UserInfo fakeUser = createUserInfo();
        fakeUser.setUsername("Fake");

        UUID id = randomUUID();
        Comment comment = new Comment();
        comment.setId(id);
        comment.setPost(post);
        comment.setCreatedBy(user);

        when(commentRepository.findById(id))
                .thenReturn(Optional.of(comment));

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication())
                .thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // Act, Assert
        assertThrows(InconformityException.class,
                () -> commentService.delete(id, fakeUser));
    }

    @Test
    public void addResponseToCommentShould_CreateResponse_WhenResponseIsValid() {
        // Arrange
        Post post = toPost();
        UserInfo user = createUserInfo();

        UUID id = randomUUID();
        Comment parentComment = new Comment();
        parentComment.setId(id);
        parentComment.setPost(post);
        parentComment.setCreatedBy(user);

        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setText("Description...");

        Comment responseToComment = new Comment();
        responseToComment.setId(randomUUID());
        responseToComment.setText(commentDTO.getText());
        responseToComment.setCreatedAt(LocalDateTime.now());
        responseToComment.setPost(post);
        responseToComment.setCreatedBy(user);
        responseToComment.setParentComment(parentComment);

        when(commentRepository.findById(id))
                .thenReturn(Optional.of(parentComment));

        // Act
        commentService.addResponseToComment(id, commentDTO, user);

        // Assert
        ArgumentCaptor<Comment> captor = forClass(Comment.class);
        verify(commentRepository).save(captor.capture());
        assertEquals(captor.getValue().getParentComment(), responseToComment.getParentComment());
    }
}