package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.ProfileVisibility;
import com.assn.demo.repos.ProfileVisibilityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.assn.demo.Helper.toVisibility;
import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProfileVisibilityServiceImplTests {

    @Mock
    ProfileVisibilityRepository visibilityRepository;

    @InjectMocks
    ProfileVisibilityServiceImpl visibilityService;

    @Test
    public void getByIdShould_ReturnVisibility_WhenVisibilityExists() {
        // Arrange
        ProfileVisibility visibility = toVisibility();

        when(visibilityRepository.findById(visibility.getId()))
                .thenReturn(Optional.of(visibility));

        // Act
        ProfileVisibility returnedVisibility = visibilityService.getById(visibility.getId());

        // Assert
        assertSame(visibility, returnedVisibility);
    }

    @Test
    public void getByIdShould_Throw_WhenIdNotExists() {
        assertThrows(EntityNotFoundException.class,
                () -> visibilityService.getById(randomUUID()));
    }

    @Test
    public void getByTypeShould_ReturnVisibility_WhenVisibilityTypeExists() {
        // Arrange
        ProfileVisibility visibility = toVisibility();

        when(visibilityRepository.findByType(anyString()))
                .thenReturn(Optional.of(visibility));

        // Act
        ProfileVisibility returnedVisibility = visibilityService.getByType(anyString());

        // Assert
        assertSame(visibility, returnedVisibility);
    }

    @Test
    public void getByTypeShould_Throw_WhenTypeNotExists() {
        assertThrows(EntityNotFoundException.class,
                () -> visibilityService.getByType(anyString()));
    }

    @Test
    public void getAllShould_ReturnAllVisibilities() {
        // Arrange
        ProfileVisibility visibility1 = toVisibility();
        ProfileVisibility visibility2 = toVisibility();
        ProfileVisibility visibility3 = toVisibility();

        List<ProfileVisibility> list = new ArrayList<>();
        list.add(visibility1);
        list.add(visibility2);
        list.add(visibility3);

        when(visibilityRepository.findAll())
                .thenReturn(list);

        // Act
        List<ProfileVisibility> returnedList = visibilityService.getAll();

        // Assert
        assertSame(list, returnedList);
    }
}
