package com.assn.demo.services;

import com.assn.demo.exeptions.DuplicateEntityException;
import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Like;
import com.assn.demo.models.Mapper;
import com.assn.demo.models.Post;
import com.assn.demo.models.UserInfo;
import com.assn.demo.repos.LikeRepository;
import com.assn.demo.repos.PostRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.assn.demo.Helper.createUserInfo;
import static com.assn.demo.Helper.toPost;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LikeServiceImplTests {

    @Mock
    LikeRepository likeRepository;

    @Mock
    PostRepository postRepository;

    @Spy
    Mapper mapper = new Mapper();

    @InjectMocks
    LikeServiceImpl likeService;

    @Test
    public void getAllUsersLikedPostShould_ReturnAllUsersLikedPost() {
        // Arrange
        Post post = toPost();
        UserInfo user1 = createUserInfo();
        UserInfo user2 = createUserInfo();
        user2.setUsername("I.S.");

        Like like1 = mapper.toLike(post, user1);
        Like like2 = mapper.toLike(post, user2);

        List<Like> likes = new ArrayList<>();
        likes.add(like1);
        likes.add(like2);

        List<UserInfo> expectedUsers = new ArrayList<>();
        expectedUsers.add(user1);
        expectedUsers.add(user2);

        when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));

        when(likeRepository.findLikesByPost(post.getId()))
                .thenReturn(likes);

        // Act
        List<UserInfo> returnedUsers = likeService.getAllUsersLikedPost(post.getId());

        // Assert
        assertSame(returnedUsers.size(), expectedUsers.size());
    }

    @Test
    public void getAllUsersLikedPostShould_Throw_WhenPostIdNotExists() {
        // Arrange
        Post post = toPost();

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> likeService.getAllUsersLikedPost(post.getId()));
    }

    @Test
    public void likePostShould_CreateLikeToPost_WhenPostAndUserAreValid() {
        // Arrange
        Post post = toPost();
        UserInfo user = createUserInfo();
        Like like = mapper.toLike(post, user);
        List<Like> likes = new ArrayList<>();
        likes.add(like);

        when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));

        when(likeRepository.findLikesByPost(post.getId()))
                .thenReturn(likes);

        // Act
        likeService.likePost(post.getId(), user);

        // Assert
        ArgumentCaptor<Like> captor = forClass(Like.class);
        verify(likeRepository).save(captor.capture());
        assertEquals(captor.getValue().isLiked(), like.isLiked());
        assertEquals(captor.getValue().getPost(), like.getPost());
    }

    @Test
    public void likePostShould_Throw_WhenPostIsAlreadyLikedByTheUser() {
        // Arrange
        Post post = toPost();
        Like like = mapper.toLike(post, post.getCreatedBy());

        when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));

        when(likeRepository.findLikeByPostAndUser(post.getId(), post.getCreatedBy().getId()))
                .thenReturn(Optional.of(like));

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> likeService.likePost(post.getId(), post.getCreatedBy()));
    }

    @Test
    public void unlikePostShould_DeleteLikeToPost_WhenPostAndUserAreValid() {
        // Arrange
        Post post = toPost();
        UserInfo user = createUserInfo();
        Like like = mapper.toLike(post, user);

        Like otherLike = mapper.toLike(post, user);
        List<Like> likes = new ArrayList<>();
        likes.add(otherLike);

        when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));

        when(likeRepository.findLikeByPostAndUser(post.getId(), user.getId()))
                .thenReturn(Optional.of(like));

        when(likeRepository.findLikesByPost(post.getId()))
                .thenReturn(likes);

        // Act
        likeService.unlikePost(post.getId(), user);

        // Assert
        verify(likeRepository, times(1))
                .delete(any(Like.class));
    }

    @Test
    public void unlikePostShould_Throw_WhenUserDidNotLikePost() {
        // Arrange
        Post post = toPost();

        when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> likeService.unlikePost(post.getId(), post.getCreatedBy()));
    }
}
