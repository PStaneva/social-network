package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Relationship;
import com.assn.demo.models.StatusType;
import com.assn.demo.models.UserInfo;
import com.assn.demo.repos.RelationshipRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.assn.demo.Helper.*;
import static com.assn.demo.enums.StatusTypeEnum.FRIENDS;
import static com.assn.demo.enums.StatusTypeEnum.PENDING;
import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RelationshipServiceImplTests {

    @Mock
    RelationshipRepository relationshipRepository;

    @InjectMocks
    RelationshipServiceImpl relationshipService;

    @Test
    public void getByIdShould_ReturnRelationship_WhenRelationshipExists() {
        // Arrange
        UserInfo sender = createUserInfo();
        sender.setUsername("Emma");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());

        Relationship relationship = toRelationship(sender, receiver, typePending);

        when(relationshipRepository.findById(relationship.getId()))
                .thenReturn(Optional.of(relationship));

        // Act
        Relationship returnedRelationship = relationshipService.getById(relationship.getId());

        // Assert
        assertSame(relationship, returnedRelationship);
    }

    @Test
    public void getByIdShould_Throw_WhenIdNotExists() {
        assertThrows(EntityNotFoundException.class,
                () -> relationshipService.getById(randomUUID()));
    }

    @Test
    public void getAllShould_ReturnAllRelationships() {
        // Arrange
        UserInfo anotherSender = createUserInfo();
        anotherSender.setUsername("Johanna");
        UserInfo sender = createUserInfo();
        sender.setUsername("Emma");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());
        StatusType typeFriends = toStatusType(FRIENDS.name());

        Relationship relationship1 = toRelationship(sender, receiver, typePending);
        Relationship relationship2 = toRelationship(anotherSender, receiver, typeFriends);

        List<Relationship> list = new ArrayList<>();
        list.add(relationship1);
        list.add(relationship2);

        when(relationshipRepository.findAll())
                .thenReturn(list);

        // Act
        List<Relationship> returnedList = relationshipService.getAll();

        // Assert
        assertSame(list, returnedList);
    }

    @Test
    public void getRelationshipStatusIdShould_ReturnRelationshipStatusId_WhenUsersAreValid() {
        // Arrange
        UserInfo sender = createUserInfo();
        sender.setUsername("Emma");
        UserInfo receiver = createUserInfo();
        StatusType typePending = toStatusType(PENDING.name());

        Relationship relationship = toRelationship(sender, receiver, typePending);

        when(relationshipRepository.getRelationshipByReceiverIdAndSenderId(receiver.getId(), sender.getId()))
                .thenReturn(relationship);

        // Act
        int returnedStatusId = relationshipService.getRelationshipStatusId(receiver.getId(), sender.getId());

        // Assert
        assertSame(typePending.getStatusId(), returnedStatusId);
    }

    @Test
    public void getRelationshipStatusIdShould_ReturnNegativeNumber_WhenNotExist() {
        // Arrange
        UserInfo sender = createUserInfo();
        sender.setUsername("Emma");
        UserInfo receiver = createUserInfo();

        // Act
        int returnedStatusId = relationshipService.getRelationshipStatusId(receiver.getId(), sender.getId());

        // Assert
        assertSame(-1, returnedStatusId);
    }
}
