package com.assn.demo.services;

import com.assn.demo.exeptions.EntityNotFoundException;
import com.assn.demo.models.Gender;
import com.assn.demo.repos.GenderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GenderServiceImplTests {

    @Mock
    GenderRepository genderRepository;

    @InjectMocks
    GenderServiceImpl genderService;

    @Test
    public void getByIdShould_ReturnGender_WhenGenderExists() {
        // Arrange
        Gender expectedGender = new Gender();
        expectedGender.setGenderId(randomUUID());
        expectedGender.setType("FEMALE");

        when(genderRepository.findById(expectedGender.getGenderId()))
                .thenReturn(Optional.of(expectedGender));

        // Act
        Gender returnedGender = genderService.getById(expectedGender.getGenderId());

        // Assert
        assertSame(expectedGender, returnedGender);
    }

    @Test
    public void getByIdShould_Throw_WhenIdNotExists() {
        assertThrows(EntityNotFoundException.class,
                () -> genderService.getById(randomUUID()));
    }

    @Test
    public void getByTypeShould_ReturnGender_WhenGenderTypeExists() {
        // Arrange
        Gender expectedGender = new Gender();
        expectedGender.setGenderId(randomUUID());
        expectedGender.setType("FEMALE");

        when(genderRepository.findByType(anyString()))
                .thenReturn(Optional.of(expectedGender));

        // Act
        Gender returnedGender = genderService.getByType(anyString());

        // Assert
        assertSame(expectedGender, returnedGender);
    }

    @Test
    public void getByTypeShould_Throw_WhenTypeNotExists() {
        assertThrows(EntityNotFoundException.class,
                () -> genderService.getByType(anyString()));
    }

    @Test
    public void getAllGendersShould_ReturnAllGenders() {
        // Arrange
        Gender gender1 = new Gender();
        gender1.setGenderId(randomUUID());
        gender1.setType("FEMALE");

        Gender gender2 = new Gender();
        gender2.setGenderId(randomUUID());
        gender2.setType("MALE");

        List<Gender> list = new ArrayList<>();
        list.add(gender1);
        list.add(gender2);

        when(genderRepository.findAll())
                .thenReturn(list);

        // Act
        List<Gender> returnedList = genderService.getAll();

        // Assert
        assertSame(list, returnedList);
    }
}
