# _Anti-Social Social Network_

<img src="images/logo2.png"  width="350" height="200">

<em>A/P 2020</em>


# _Project Description_
> Anti-Social Social Network (ASSN) is a web application that enables you to:
>   * connect with people
>   * create, comment and like posts
>   * get a feed of the most relevant posts of your connections

1. **Registration**\
 To become a member of ASSN, user must fill the registration form and then confirm his email on Mailtrap.io.\
Mailtrap.io is a fake smtp server that provides testing, viewing and sharing emails sent from the development environments without spamming. 

2.  **Connection statuses**\
The connection statuses could be PENDING, FRIENDS and BLOCKED.
    - A user_1 could send a friend request to user_2.
    - A user_1 could also cancel friend request.
    - A user_2 could accept or reject friend request.
    - Both users could unfriend, block or unblock a specified user.

3. **Profile Visibility**\
The user profile could be visible for 'EVERYONE', 'ONLY ME', 'CONNECTED PROFILES'.\
When a user registers in our social network, his profile is visibile for 'ONLY ME' by default.\
To set profile visibility, user should update his detailed information.

# _Inspiration_
> Anti-Social Social Network is targeted to people who are in state of emergency over coronavirus.\
> People who preferred to #StayHome and avoid going out even when it wasn't that mainstream.

# _Getting Started_
1. **Required Tools**
    *  IntelliJ Idea
    *  JDK version 1.8
    *  MySQL Server

2. **Check out the project** 
     * From the main menu in IntelliJ Idea, choose VCS | Get from Version Control
     * In the Get from Version Control dialog, choose GitHub on the left
     * Specify the URL of the [repository](https://gitlab.com/PStaneva/social-network.git) that you want to clone
     * In the Directory field, enter the path to the folder where your local Git repository will be created
     * Click Clone

3. **Set up the database**
     * Run ASSN database [script](assn_database_overview/database_script.txt) in your MySQL Server

4. **Run the application**
     * The URL for our kinda-social-network is: **http://localhost:8080/**


# _Screenshots_

* _Home Page_

<img src="images/home-page.png"  width="500" height="300">

* _Login Form_

<img src="images/login-form-users.png"  width="500" height="300">

* _User Profile_

<img src="images/user-profile.png"  width="500" height="300">

* _Post_

<img src="images/post.png"  width="350" height="450">




# _Trello_
https://trello.com/invite/b/mN9Wma35/0b2824da53dcfcda450045a6a9f6bb7e/social-network

